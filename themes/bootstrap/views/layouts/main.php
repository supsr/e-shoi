<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="language" content="en" />
            <style type="text/css">
                @font-face {
                    font-family: "TH Sarabun New";
                    src: url('../css/THSarabunNew.woff') format("woff");

                }
                @font-face {
                    font-family: "TH Sarabun New";
                    src: url('../css/THSarabunNew<?php echo " "; ?>Italic.woff') format("woff");
                    font-style: italic;

                }
                @font-face {
                    font-family: "TH Sarabun New";
                    src: url('../css/THSarabun<?php echo " "; ?>Bold.woff') format("woff");
                    font-weight: bold; 

                }
                @font-face {
                    font-family: "TH Sarabun New";
                    src: url('../css/THSarabunNew<?php echo " "; ?>BoldItalic.woff') format("woff");
                    font-weight: bold; 
                    font-style: italic;
                }

                #thaiFont {
                    font-family: "TH Sarabun New";
                    font-size: 22px;
                }

                .thaiArticle {
                    text-indent: 3em;
                    text-align:justify;
                    text-justify:inter-word;
                }


            </style>

            <title><?php echo CHtml::encode($this->pageTitle); ?></title>
            <?php Yii::app()->bootstrap->register(); ?>
    </head>

    <body>
        <?php
        $reportAc = Abuse::model()->findAll();
        $badge = $this->widget('bootstrap.widgets.TbBadge', array(
            'type' => 'important',
            'encodeLabel' => 'false',
            'label' => (count(Approver::getPendingAccounts()) > 0) ? '!' : '',
                ), true);

        $this->widget('bootstrap.widgets.TbNavbar', array(
            'brand' => Yii::app()->name,
            'brandUrl' => (isset(Yii::app()->params['hosting']))? "/post/index" : "/" . strtolower(Yii::app()->name) . "/post/index",
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'encodeLabel' => false,
                    'items' => array(
                        array('label' => 'Home', 'url' => array('post/index')),
                        //array('label' => 'Contact', 'url' => array('site/contact')),
                        array('label' => 'Members' . ((Yii::app()->user->getState("isAdmin")) ? ' ' . $badge : ''), 'url' => array('member/index'),
                        ),
                    //array('label' => 'About', 'url' => array('site/page', 'view' => 'about')),
                    ),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'htmlOptions' => array('class' => 'pull-right'),
                    'items' => array(
                        array('label' => 'Register', 'url' => array('member/register'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Login', 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => ' ' . Yii::app()->user->name . '',
                            'url' => array('site/login'),
                            'visible' => !Yii::app()->user->isGuest,
                            'icon' => 'icon-user',
                            'items' => array(
                                array('label' => 'View profile', 'url' => array('member/viewMemberDetail'), 'icon' => 'icon-edit'),
                                array('label' => 'My posts', 'url' => array('post/viewPostList'), 'icon' => 'icon-list-alt'),
                                '---',
                                array('label' => 'Logout', 'url' => array('/site/logout'), 'icon' => 'icon-off', 'visible' => !Yii::app()->user->isGuest),
                            )),
                    ),
                ),
            ),
        ));
        ?>



        <div class="container" id="page">
            <div id="header">

            </div><!-- header -->



            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                <div style="float: left; margin-top: 20px">All Rights Reserved.&nbsp;Copyright &copy; <?php echo date('Y'); ?> by E-shoi The gang.<br/></div>
                <div style="float: right; margin-top: 20px"><?php echo Yii::powered(); ?></div>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
