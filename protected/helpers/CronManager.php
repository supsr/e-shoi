<?php

class CronManager {

    public static function addCron($idAuction, $timeToClose) {
        $cron = new Crontab('my_crontab');
        $datetime = new DateTime($timeToClose);
        $second = $datetime->format('s');
        if (intval($second) >= 30) {
            $datetime = new DateTime($timeToClose);
            date_add($datetime, new DateInterval("PT1M"));
        }
        $date = $datetime->format('d');
        $month = $datetime->format('m');
        $hour = $datetime->format('H');
        $minute = $datetime->format('i');

        $job = new CronApplicationJob('yiicmd', 'closeac', array("" . $idAuction), $minute, $hour, $date, $month); // run every day
        $cron->add($job);
        $cron->saveCronFile();
        $cron->saveToCrontab();
    }

}
