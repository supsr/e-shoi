<?php

class EANCreator{
    public static function createCode($k1, $k2, $k3){
        $strCode = str_pad($k1, 6, "0", STR_PAD_LEFT) . str_pad($k2, 3, "0", STR_PAD_LEFT) . str_pad($k1, 3, "0", STR_PAD_LEFT);
        return $strCode;
    }
}