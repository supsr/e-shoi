<?php

class Approver {

    public static function approve($approve = true, $idMember) {
        $approvedUser = Member::model()->findByPk($idMember);
        $approvedUser->status = ($approve) ? 'enable' : 'disable';
        if ($approvedUser->save()) {
            if($approve) $msg = "Member named '" . $approvedUser->firstname . ' ' . $approvedUser->lastname . "' has been approved registration!";
            else $msg = "Member named '" . $approvedUser->firstname . ' ' . $approvedUser->lastname . "' has been declined the registration!";
            $typeOfMsg = 1;
        } else {
            $msg = $approvedUser->getErrors();
            $typeOfMsg = 0;
        }
        return array("msg"=>$msg, "typeOfMsg"=>$typeOfMsg);
    }
    
    public static function getPendingAccounts(){
        return Member::model()->findAllByAttributes(array('status' => 'unactivated'));
    }
}

?>
