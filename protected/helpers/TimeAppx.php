<?php

class TimeAppx {
    public static function appx($timeToClose, $timeUnit = 15){
        $datetime = new DateTime($timeToClose);
        $date = intval($datetime->format('d'));
        $month = intval($datetime->format('m'));
        $hour = intval($datetime->format('H'));
        $minute = intval($datetime->format('i'));
        $second = intval($datetime->format('s'));
        $minute15 = $minute % $timeUnit;
        $secondMore = (60 * $minute15) + $second ;
        $halfOf15M = ceil(15 * $timeUnit / 2);
        if($secondMore >= $halfOf15M){ // ceiling
            $secondTogo = ($timeUnit * 60) - $secondMore;
            $minuteTogo = (int)($secondTogo / 60);
            $secondTogo = $secondTogo % 60;
            date_add($datetime, new DateInterval("PT" . $minuteTogo . "M" . $secondTogo . "S"));
        } else { // floor
            $minuteTogo = (int)($secondMore / 60);
            $secondTogo = $secondMore % 60;
            date_sub($datetime, new DateInterval("PT" . $minuteTogo . "M" . $secondTogo . "S"));
        }
        return $datetime->format('Y-m-d H:i:s');
    }
}