<?php

class InfoMemFinder {

    public function getScore($id) {
        /*
          $criteria = new CDbCriteria;
          $criteria->select = "m.idMember, scoreFromSeller";
          $criteria->alias = 'm';
          $criteria->join = "RIGHT JOIN post ON (m.idMember = post.Seller_Member_idMember AND m.idMember = $id)";
          $criteria->join .= " RIGHT JOIN feedback ON post.idPost = feedback.Post_idPost";
          $criteria->group = "scoreFromSeller";
          $sellerRating = Member::model()->findAll($criteria);
          return $sellerRating;
         */
        $ratingSeller = Yii::app()->db
                        ->createCommand('SELECT idMember, AVG(scoreFromBuyer)
FROM ((member as m JOIN post as p ON (m.idMember = p.Seller_Member_idMember AND m.idMember = ' . $id . ')) JOIN feedback as f ON p.idPost = f.Post_idPost) GROUP BY  scoreFromBuyer')->QueryRow();
        
        $ratingBuyer = Yii::app()->db
                        ->createCommand('SELECT idMember, AVG(scoreFromSeller)
FROM ((member as m JOIN post as p ON (m.idMember = p.Buyer_Member_idMember AND m.idMember = ' . $id . ')) JOIN feedback as f ON p.idPost = f.Post_idPost) GROUP BY  scoreFromSeller')->QueryRow();
        
        return array('s'=>$ratingSeller['AVG(scoreFromBuyer)'], 'b'=>$ratingBuyer['AVG(scoreFromSeller)']);
    }

    public function getPostCount($id){
        $postSCount = Post::model()->countByAttributes(array('Seller_Member_idMember'=>$id));
        $postBCount = Post::model()->countByAttributes(array('Buyer_Member_idMember'=>$id));
        $postSBCount = Post::model()->countByAttributes(array('Seller_Member_idMember'=>$id, 'Buyer_Member_idMember'=>$id));
        return $postSCount + $postBCount - $postSBCount;
    }
}
