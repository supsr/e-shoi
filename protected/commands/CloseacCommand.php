<?php 
 
class CloseacCommand extends CConsoleCommand
{
    public function run($args)
    {
        $idPost = $args[0];
        $post = Post::model()->findByPk($idPost);
        $post->status = 'Purchased';
        $post->save();
        $invoice = new Invoice();
        $invoice->Post_idPost = $idPost;
        $invoice->status = 'Wait for Paying';
        $invoice->save();
    }
}