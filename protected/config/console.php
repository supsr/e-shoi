<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Console Application',
    'import' => array(
        'application.models.*',
    ),
    // preloading 'log' component
    'preload' => array('log'),
    // application components
    'components' => array(
        'db' => array(
            'class' => 'CDbConnection',
            'charset' => 'utf8',
            'connectionString' => 'mysql:host=localhost;dbname=psrinth_eshoi',
            'username' => 'root',
            'password' => ''
        ),
//        'db' => array(
//            'connectionString' => 'mysql:host=localhost;dbname=psrinth_eshoi',
//            'emulatePrepare' => true,
//            'username' => 'root',
//            'password' => '',
////            'username' => 'psrinth_robot3',
////            'password' => 'EAn1ohKW',
//            'charset' => 'utf8',
//        ),
        // usefull for generating links in email etc...
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => FALSE,
            'rules' => array(),
        ),
        // uncomment the following to use a MySQL database
        /*
          'db'=>array(
          'connectionString' => 'mysql:host=localhost;dbname=testdrive',
          'emulatePrepare' => true,
          'username' => 'root',
          'password' => '',
          'charset' => 'utf8',
          ),
         */
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
);
