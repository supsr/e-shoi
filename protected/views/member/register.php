<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name . ' - Register';

/*
  $this->breadcrumbs=array(
  'Site'=>array('/site'),
  'Register',
  ); */
?>

<?php
/*
  <h1><?php echo $this->id . '/' . $this->action->id; ?></h1>
 */

function alert($msg) {
    echo '<script language="javascript">';
    echo 'alert("' . $msg . '")';
    echo '</script>';
}
?>
<p>
<tt><?php //echo __FILE__;       ?></tt>
</p>

<?php
if (isset($invalid) && $invalid == 1) {
    $msg = "";
    $typeOfMsg = 0;
    if (isset($error->username) && $error->username == 1)
        $msg = $msg . " - Username";
    if (isset($error->password) && $error->password == 1)
        $msg = $msg . " - Password";
    if (isset($error->password) && $error->password == 1)
        $msg = $msg . " - Re-password";
    if (isset($error->firstname) && $error->firstname == 1)
        $msg = $msg . " - Firstname";
    if (isset($error->lastname) && $error->lastname == 1)
        $msg = $msg . " - Lastname";
    if (isset($error->citizenID) && $error->citizenID == 1)
        $msg = $msg . " - Citizen ID";
    if (isset($error->address) && $error->address == 1)
        $msg = $msg . " - Address";
    if (isset($error->country) && $error->country == 1)
        $msg = $msg . " - Country";
    if (isset($error->telephonenumber) && $error->telephonenumber == 1)
        $msg = $msg . " - Telephone number ";
    if (isset($error->email) && $error->email == 1)
        $msg = $msg . " - E-mail";
} else if (isset($invalid) && $invalid == 0) {
    $msg = "คุณสามารถตรวจสอบผลการสมัครสมาชิกได้ที่อีเมลล์ " . $guest->email;
    $typeOfMsg = 1;
}
?>
<div class="mainRegisterPage_div">
    <div class="form">
        <div class="head_RegisterPage">
            <h1> Register</h1>
            <p class="note">โปรดกรอกข้อมูลเพื่อสมัครสมาชิก</p>
        </div>
<?php
if (isset($msg)) {
    if ($typeOfMsg == 1) {
        ?>
                <div style="display: block;">
                    <div class="alert alert-success" style="margin: 15px 50px;">
                        <strong>Thank you for registering!</strong><br><?php echo $msg; ?>
                    </div>
                </div>
    <?php } else { ?>
                <div style="display: block;">
                    <div class="alert alert-error" style="margin: 15px 50px;">
                        <strong>Error! </strong>ข้อมูลผิดพลาด โปรดกรอกข้อมูลใหม่อีกครั้ง<br><?php echo $msg; ?>
                    </div>
                </div>
        <?php
    }
}
?>
        <form method="POST" action="create" class="form-horizontal">
            <div style="display: block; height: 350px">
                <div class="column1_RegisterPage">
                    <br>
                    <div class="control-group"><label class="control-label required" for="Member_username">Username <span class="required">*</span></label><div class="controls"><input name="username" value="<?php if (isset($guest->username)) echo $guest->username; ?>" id="Member_username" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_username_em_" style="display: none;"></span></div></div>
                    <div class="control-group"><label class="control-label required" for="Member_password">Password <span class="required">*</span></label><div class="controls"><input name="password" id="Member_password" type="password" maxlength="45" required=""><span class="help-inline error" id="Member_password_em_" style="display: none;"></span></div></div>   
                    <div class="control-group"><label class="control-label required" for="Member_repassword">Re-password <span class="required">*</span></label><div class="controls"><input name="repassword" id="Member_repassword" type="password" maxlength="45" required=""><span class="help-inline error" id="Member_repassword_em_" style="display: none;"></span></div></div>
                    <div class="control-group"><div class="alert alert-info"><strong>Notice!</strong>&nbsp;Your password must be 6-20 characters length.</div></label><div class="controls"></div></div>


    <!--div class="control-group success"><label class="control-label required" for="Member_password">Re-Password <span class="required">*</span></label><div class="controls"><input name="repassword" id="Member_repassword" type="password" maxlength="45"><span class="help-inline error" id="Member_password_em_" style="display: none;"></span></div></div>  -->              
                </div>
                <div class="column2_RegisterPage">
                    <br>
                    <div class="control-group "><label class="control-label required" for="Member_firstname">First name <span class="required">*</span></label><div class="controls"><input class="" name="firstname" value="<?php if (isset($guest->firstname)) echo $guest->firstname; ?>"id="Member_firstname" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_firstname_em_" style="display: none"></span></div></div>                
                    <div class="control-group "><label class="control-label required" for="Member_lastname">Last name <span class="required">*</span></label><div class="controls"><input class="" name="lastname" value="<?php if (isset($guest->lastname)) echo $guest->lastname; ?>"id="Member_lastname" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_lastname_em_" style="display: none"></span></div></div>
                    <div class="control-group "><label class="control-label required" for="citizenID">Citizen ID <span class="required">*</span></label><div class="controls"><input class="" name="citizenID" value="<?php if (isset($guest->citizenID)) echo $guest->citizenID; ?>" type="text" maxlength="13" required=""><span class="help-inline error" id="Member_citizenID_em_" style="display: none"></span></div></div>
                    <div class="control-group "><label class="control-label required" for="Member_address">Address <span class="required">*</span></label><div class="controls"><input class="span4" name="address" value="<?php if (isset($guest->address)) echo $guest->address; ?>"id="Member_address" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_address_em_" style="display: none"></span></div></div>
                    <div class="control-group "><label class="control-label required" for="Member_country">Country <span class="required">*</span></label><div class="controls"><input class="" name="country" value="<?php if (isset($guest->country)) echo $guest->country; ?>"id="Member_country" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_country_em_" style="display: none"></span></div></div>                
                    <div class="control-group "><label class="control-label required" for="Member_telephonenumber">Telephone number <span class="required">*</span></label><div class="controls"><input class="" name="telephonenumber" value="<?php if (isset($guest->telephonenumber)) echo $guest->telephonenumber; ?>"id="Member_telephonenumber" type="text" maxlength="10" required=""><span class="help-inline error" id="Member_telephonenumber_em_" style="display: none"></span></div></div>                
                    <div class="control-group "><label class="control-label required" for="Member_email">E-mail <span class="required">*</span></label><div class="controls"><input class="" name="email" value="<?php if (isset($guest->email)) echo $guest->email; ?>"id="Member_email" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_email_em_" style="display: none"></span></div></div>
                </div>
            </div>
            <div class="form-actions" style="display: block; text-align: center">
                <input id="submitBTN" class="btn btn-primary btn-large" type="submit" value="Submit" name="B1">
                <input id="resetBTN" class="btn btn-danger" type="reset" value="Reset" name="B2">
            </div>
        </form>
    </div>
</div>
<?php
if (isset($usernameDuplicate) && $usernameDuplicate == 1) {
    alert("มีผู้ใช้ username นี้แล้ว โปรดกรอกข้อมูลใหม่อีกครั้ง");
}
?>

