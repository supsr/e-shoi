<?php
function strTel($strNumber) {

        if (is_numeric($strNumber) && strlen($strNumber) > 8 && strlen($strNumber) < 11) {
            $i = strlen($strNumber);
            $newStrNumber = "";
            while ($i >= 4) {
                $newStrNumber = "-" . substr($strNumber, $i - 4, 4) . $newStrNumber;
                $i -= 4;
            }
            $newStrNumber = substr($strNumber, 0, $i) . $newStrNumber;
            return $newStrNumber;
        } else {
//            echo strpos($strNumber, "-");
            if (strlen($strNumber) > 10 && strlen($strNumber) < 13 && preg_match_all('/^[0-9]{2,3}[-]{0,1}[0-9]{3,4}[-]{0,1}[0-9]{4,4}$/i', $strNumber)) {
                $newStrNumber = str_replace("-", "", $strNumber);
            }
            return strTel($newStrNumber);
        }
}
?>

<p>
<tt><?php //echo __FILE__;            ?></tt>
</p>


<div class="main_div">
    <?php
    if (isset($reportAccount) && $reportAccount == 1) {
        ?>
        <div style="display: block;">
            <div class="alert alert-success" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Thank you for report problem. </strong><br>เจ้าหน้าที่จะทำการตรวจสอบและแก้ปัญหาให้โดยทันที<br>
            </div>
        </div>
    <?php } else if (isset($reportAccount) && $reportAccount == 0) { ?>
        <div style="display: block;">
            <div class="alert alert-error" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Report Fail!</strong><br>โปรดกรอกรายละเอียดของปัญหาที่จะร้องเรียนอีกครั้ง<br>
            </div>
        </div>

    <?php } ?>

    <?php
    if (isset($_GET['reportAccount']) && $_GET['reportAccount'] == 1) {
        ?>
        <div style="display: block;">
            <div class="alert alert-success" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Thank you for report problem. </strong><br>เจ้าหน้าที่จะทำการตรวจสอบและแก้ปัญหาให้โดยทันที<br>
            </div>
        </div>
    <?php } else if (isset($_GET['reportAccount']) && $_GET['reportAccount'] == 0) { ?>
        <div style="display: block;">
            <div class="alert alert-error" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Report Fail!</strong><br>โปรดกรอกรายละเอียดของปัญหาที่จะร้องเรียนอีกครั้ง<br>
            </div>
        </div>

    <?php } ?>

    <?php
    if (isset($_GET['msg'])) {
        if ($_GET['typeOfMsg'] == 1) {
            ?>
            <div class="alert alert-info">
                <strong>Done!</strong> <?php echo $_GET['msg']; ?>
            </div>
        <?php } else {
            ?>
            <div class="alert alert-error">
                <strong>Error!</strong> <?php echo $_GET['msg']; ?>
            </div>
            <?php
        }
    }
    ?>
    <?php
    if (isset($resultApproval)) {
        if ($resultApproval['typeOfMsg'] == 1) {
            ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong>
                <?php
                echo $resultApproval['msg'];
            } else {
                ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Error!</strong>
                    <?php
                    echo $resultApproval['msg'];
                }
                ?>
            </div>
        <?php } ?>

        <h2><?php echo $memberProfile->username; ?></h2>
        <hr />

        <h4>Profile</h4>
        <div class="column1_div">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <td width="150">Name</td>
                        <td width="550"><?php echo $memberProfile->firstname . '  ' . $memberProfile->lastname; ?></td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>Address</td>
                        <td><?php echo $memberProfile->address; ?></td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td><?php echo $memberProfile->country; ?></td>
                    </tr>
                    <?php
                    $imf = new InfoMemFinder();
                    $ratingSale = $imf->getScore($memberProfile->idMember);
                    if (empty($ratingSale['s'])) {
                        $ratingSale['s'] = "-";
                    } else {
                        $ratingSale['s'] = number_format($ratingSale['s'], 2);
                    }
                    if (empty($ratingSale['b'])) {
                        $ratingSale['b'] = "-";
                    } else {
                        $ratingSale['b'] = number_format($ratingSale['b'], 2);
                    }
                    ?>

                    <tr>
                        <td>Seller Rating</td>
                        <td><?php echo $ratingSale['s']; ?></td>
                    </tr>
                    <tr>
                        <td>Buyer Rating</td>
                        <td><?php echo $ratingSale['b']; ?></td>
                    </tr>
                    <tr>
                        <td>Telephone Number</td>
                        <td><?php echo strTel($memberProfile->telephonenumber); ?></td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td><?php echo $memberProfile->email; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="column2_div">

            <table class="table" style="width: 200px">

                <?php if (!isset($_GET['idMember']) || (Yii::app()->user->getState('cID') == $memberProfile->idMember)) { ?>
                    <tr><td><a class="btn-block btn btn-large" href="editProfile"><i class="icon-pencil"></i>&nbsp;Edit my profile</a></td></tr>
                    <?php $person = "my"; ?>
                    <?php
                } else {
                    $person = $memberProfile->username . "'s";
                    if (strlen($person) > 10) {
                        $person = substr($person, 0, 7) . "...'s";
                    }
                }
                ?> 
                <tr><td><a class="btn-block btn btn-large" href="../post/viewPostList<?php echo '?idMember=' . $memberProfile->idMember; ?>">View <?php echo $person; ?> post</a></td></tr>
                <tr><td></td></tr>
                <tr class="warning"><td style="text-align: center">Status : 
                        <?php if ($memberProfile->status == 'unactivated') { ?>
                            <span class="label label-warning">pending</span>
                        <?php } else if ($memberProfile->status == 'disable') { ?>
                            <span class="label label-important">banned</span>
                        <?php } else if ($memberProfile->status == 'enable') { ?>
                            <span class="label label-success">active</span>
                        <?php } ?>
                    </td>
                </tr>
                <?php if (Yii::app()->user->getState('isAdmin') && ($memberProfile->status == 'unactivated')) { ?>
                    <?php $referenceAccountID = (isset($_GET['idMember'])) ? $_GET['idMember'] : Yii::app()->user->getState("cID"); ?>
                    <tr class="warning" align="center"><td><div class="btn-group"><a class="btn btn-success" href="ViewMemberDetail?idMember=<?php echo $referenceAccountID; ?>&approve=1"><i class="icon-ok icon-white"></i>&nbsp;Approve</a><a class="btn btn-danger" href="ViewMemberDetail?idMember=<?php echo $referenceAccountID; ?>&approve=0"><i class="icon-remove icon-white"></i>&nbsp;Decline</a></div></td></tr>
                    <tr><td></td></tr>
                <?php } ?>

                <?php if (Yii::app()->user->getState('isAdmin') && ($memberProfile->status == 'enable') && (isset($_GET['idMember']) && $_GET['idMember'] != Yii::app()->user->getState("cID"))) { ?>
                    <tr><td><a href="ban?idMember=<?php echo (isset($_GET['idMember'])) ? $_GET['idMember'] : Yii::app()->user->getState("cID"); ?>">Ban this account</a></td></tr>
                <?php } ?>

                <tr><td>
                        <?php if (isset($_GET['idMember']) && $_GET['idMember'] != Yii::app()->user->getState("cID")) { ?>
                            <div class="controls">
                                <a href="#report" role="button" data-toggle="modal">Report this account</a>
                            </div>
                            <div id="report" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordTitle" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="changePasswordLabel">Report this account</h3>
                                </div>
                                <form class="form-horizontal" method="post" action="reportaccount">
                                    <div class="modal-body">
                                        <div class="control-group">
                                            <label class="control-label required" for="Member_username">ร้องเรียน</label>
                                            <div class="controls">
                                                <input name="username" disabled="disabled" value="<?php echo $memberProfile->username; ?>" type="text">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">รายละเอียด</label>
                                            <div class="controls">
                                                <textarea rows="5" type="text" name="message"></textarea>
                                            </div>
                                        </div>
                                        
                                        <input type="hidden" name="referred_idMember" value="<?php echo $memberProfile->idMember; ?>">
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                        <button class="btn btn-primary btn-large" type="submit">Report</button>
                                    </div>
                                </form>
                            </div>
                        <?php } ?>
                    </td></tr>
            </table>
        </div>

    </div>










