<!-- viewPendingList -->
<?php
/* @var $this MemberController */

$currentUrl = Yii::app()->request->url;

function pendingAmount($amonut) {
    echo $amonut . " Request ";
}

function generateViewProfileUrlFromMemberName($AccountName) {
    $currentUrl;
    echo "?AccountName=" . $AccountName;
}

function generateViewProfileUrlFromPendingId($pendingId) {
    echo "member/viewMemberDetail";
    echo "?memberId=" . $pendingId;
}

function showPendingAccount($pendingId, $accountName, $firstname, $lastname, $email, $tel, $registerDate) {
    echo '<tr>
                    <td style="text-align:center">
                        ' . $pendingId . '
                    </td>
                    <td style="text-align:center"> <a href="';
    generateViewProfileUrlFromPendingId($pendingId);
    echo '">' . $accountName . '</a> </td>
                    <td>' . $firstname . '</td>
                    <td>' . $lastname . '</td>
                    <td style="text-align:center">' . $email . '</td>
                    <td style="text-align:center">' . $tel . '</td>
                    <td style="text-align:center">' . $registerDate . '</td>
                    <td style="text-align:center">
                    	<a class="btn btn-success btn-xs">Approve</a>
    				</td>
    				<td style="text-align:center">
                    	<a class="btn btn-danger btn-xs">Reject</a>
    				</td>
                </tr>';
}
?>

<p>
<tt><?php //echo __FILE__;   ?></tt>
</p>




<!-- end of viewPendingList -->    

<!-- viewMemberList --> 
<?php
/* @var $this MemberController */
$currentUrl = Yii::app()->request->url;

function generateUrlFromMemberId($memberId) {
    echo "member/viewMemberDetail";
    echo "?memberId=" . $memberId;
}

function generateSearchMenberUrlFromMemberName($memberName) {
    echo "member/viewMemberDetail";
    echo "?memberName=" . $memberName;
}

function showMember($id, $memberName, $totalPost, $sellerRating, $buyerRation, $duration) {
    echo '<tr>
                    <td style="text-align:center">
                        ' . $id . '
                    </td>
                    <td> ' . $memberName . ' </td>
                    <td style="text-align:center">' . $totalPost . '</td>
                    <td style="text-align:center">' . $sellerRating . '</td>
                    <td style="text-align:center">' . $buyerRation . '</td>
                    <td style="text-align:center">' . $duration . '</td>
                    <td style="text-align:center">
                    	<a class="btn btn-warning btn-xs" href=';
    generateUrlFromMemberId($id);
    echo'>viewProfile</a>
    				</td>
                </tr>';
}
?>

<p>
<tt><?php //echo __FILE__;   ?></tt>
</p>

<!-- end of viewMemberList -->


<ul class="nav nav-tabs">
    <li><a href="#home" data-toggle="tab">Member</a></li>
    <?php if (isset($_GET['typeMember']) && ($_GET['typeMember'] == 'pending')) { ?>
        <li>
            <a href="#profile" data-toggle="tab">Pending
            </a>
        </li>
    <?php } ?>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="home">
        <div class="accordion-heading">
            <b class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" align="center">
                <h1 align="left">SearchMember</h1><form class="form-search">
                    <div align="left">
                        
                            <div class="input-append">
                              
                                <input type="text" class="input-xlarge search-query" id="appendedInputButton" placeholder="Member's name" />
                                <button type="submit" class="btn"><i class="icon-search"></i></button>
                            </div>
                        

                    </div>
                </form>
            </b>
        </div>
        <div class="main_div"> 
            <p>
            <table class="table">
                <thead>
                    <tr>
                        <th class="span1" bgcolor="#e74c3c" style="color:white">
                <div align="center" >id</div>
                </th>
                <th class="span3" bgcolor="#e74c3c" style="color:white">Member Name</th>
                <th class="span1" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Post</div>
                </th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Seller Rating</div>
                </th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Buyer Rating</div>
                </th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center">Duration</div>
                </th>
                <th class="span1" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center">Profile</div>
                </th>
                </tr>
                </thead>
                <tbody>
                    <?php showMember("01234", "pakko", 12, 4.12, 3.11, "2ปี 4เดือน 6วัน") ?>
                    <?php showMember("01235", "pakky", 12, 3.12, 2.11, "2ปี 3เดือน 16วัน") ?>
                    <?php showMember("01236", "pakkey", "-", "-", 4.11, "1ปี 4เดือน 26วัน") ?>

                </tbody>
            </table>
            </p>
        </div>

    </div>
    <div class="tab-pane" id="profile">
        <div class="main_div">
            <p>
            <h2>Pending Accounts</h2>
            <?php pendingAmount(10); ?>

            <table class="table">
                <thead>
                    <tr>
                        <th class="span1" bgcolor="#e74c3c" style="color:white">
                <div align="center" >PendingId</div>
                </th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Account Name</div>
                </th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">Firstname</th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">Lastname</th>

                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Email</div>
                </th>
                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Tel</div>
                </th>

                <th class="span2" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center">RegistrationDate</div>
                </th>
                <th class="span1" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center" >Approve</div>
                </th>
                <th class="span1" bgcolor="#e74c3c" style="color:white">
                <div id="type"align="center">Reject</div>
                </th>
                </tr>
                </thead>
                <tbody>
                    <?php showPendingAccount("01234", "pakko", "Nattapat", "Khu", "pakko@hotmail.com", "089XXXXXXX", "7 ตุลาคม 2557"); ?>
                    <?php showPendingAccount("01235", "benzo", "Kittipat", "Hutyoo", "benzo@hotmail.com", "089XXXXXXX", "7 ตุลาคม 2557"); ?>
                    <?php showPendingAccount("01236", "misterk", "Kankawin", "Inthehole", "misterk@hotmail.com", "089XXXXXXX", "7 ตุลาคม 2557"); ?>
                    <?php showPendingAccount("01237", "fapoo", "Kritsana", "Prayaka", "fapoo@hotmail.com", "089XXXXXXX", "7 ตุลาคม 2557"); ?>
                    <?php showPendingAccount("01238", "vittamin", "Chanavit", "Kitsun", "vittamin@hotmail.com", "089XXXXXXX", "7 ตุลาคม 2557"); ?>
                    <?php showPendingAccount("01238", "suonsale", "Parawit", "Pichitluu", "suonsale@hotmail.com", "089XXXXXXX", "7 ตุลาคม 2557"); ?>


                </tbody>
            </table>
            </p>
        </div>



    </div>
</div>


