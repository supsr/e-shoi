<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */


?>
<div class="bgLoginPage_div">
<div class="headLoginPage_div">
<h1 >Login</h1>

<p>โปรดกรอกข้อมูลเพื่อเข้าสู่ระบบ</p>
</div>

<div class="mainLoginPage_div">

<div class="form">

	


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
    'type'=>'inline',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
	'validateOnSubmit'=>true,
	),
)); ?>    

		<div class="control-group">
		  <div class="controls">
		    <div class="input-prepend">
		      <span class="add-on"><i class="icon-user"></i></span>
		      	<?php echo $form->textFieldRow($model,'username',array(
				'class'=>'span2',
				'placeholder' => 'Username')); ?>
	</div></div></div>

		<div class="control-group">
		  <div class="controls">
		    <div class="input-prepend">
		      <span class="add-on"><i class="icon-lock"></i></span>
				<?php echo $form->passwordFieldRow($model,'password',array(
						'class'=>'span2',
				        'placeholder'=>'password',
				    )); ?>		    
			</div>
		  </div>
		</div>




	<?php //echo "<br>";?>

	

	

	<?php 
		echo $form->checkBoxRow($model,'rememberMe'); 
	?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Login',
            'size' => 'large',
        )); ?>

        <br><br>
        <div class="pull-right">
         <?php $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
		    'id'=>'mydialog',
		    'options'=>array(
		        'title'=>'Forgot your password?',
		        'autoOpen'=>false,
		    ),

		));
			echo "โปรดกรอกข้อมูลเพื่อขอรหัสผ่าน";
		    echo $form->textFieldRow($model,'username');
		    $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Send',
        ));
		$this->endWidget('zii.widgets.jui.CJuiDialog');
		/** End Widget **/
		echo CHtml::link('Forgot your password?', '#', array(
		   'onclick'=>'$("#mydialog").dialog("open"); return false;',
		));?>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

</div>

</div>