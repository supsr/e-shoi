<!-- viewPendingList -->
<?php
/* @var $this MemberController */

$currentUrl = Yii::app()->request->url;
function strTel($strNumber) {

        if (is_numeric($strNumber) && strlen($strNumber) > 8 && strlen($strNumber) < 11) {
            $i = strlen($strNumber);
            $newStrNumber = "";
            while ($i >= 4) {
                $newStrNumber = "-" . substr($strNumber, $i - 4, 4) . $newStrNumber;
                $i -= 4;
            }
            $newStrNumber = substr($strNumber, 0, $i) . $newStrNumber;
            return $newStrNumber;
        } else {
//            echo strpos($strNumber, "-");
            if (strlen($strNumber) > 10 && strlen($strNumber) < 13 && preg_match_all('/^[0-9]{2,3}[-]{0,1}[0-9]{3,4}[-]{0,1}[0-9]{4,4}$/i', $strNumber)) {
                $newStrNumber = str_replace("-", "", $strNumber);
            }
            return strTel($newStrNumber);
        }
}
    
function pendingAmount($amount) {
    if ($amount == 1)
        echo $amount . " request";
    else
        echo $amount . " requests";
}

function generateViewProfileUrlFromPendingId($pendingId) {
    echo "viewMemberDetail";
    echo "?idMember=" . $pendingId;
}

function showPendingAccount($pendingId, $accountName, $firstname, $lastname, $email, $tel, $registerDate) {
    echo '<tr>
    <td style="text-align:center">
    ' . $pendingId . '
    </td>
    <td> <a href="';
    generateViewProfileUrlFromPendingId($pendingId);
    echo '">' . $accountName . '</a> </td>
    <td>' . $firstname . '</td>
    <td>' . $lastname . '</td>
    <td style="text-align:center">' . $email . '</td>
    <td style="text-align:center">' . $tel . '</td>
    <td style="text-align:center">' . $registerDate . '</td>
    <td style="text-align:center">
    <div class="btn-group">
    <a class="btn btn-success btnApprove" href="index?idMember=' . $pendingId . '&approve=1"><i class="icon-ok icon-white"></i></a>
    <a class="btn btn-danger btnDecline" href="index?idMember=' . $pendingId . '&approve=0"><i class="icon-remove icon-white"></i></a>
    </div></td>
    </tr>';
}
?>

<p>
<tt><?php //echo __FILE__;                             ?></tt>
</p>




<!-- end of viewPendingList -->    

<!-- viewMemberList --> 
<?php
/* @var $this MemberController */
$currentUrl = Yii::app()->request->url;

function generateUrlFromMemberId($memberId) {
    echo "viewMemberDetail";
    echo "?idMember=" . $memberId;
}

function showMember($id, $memberName, $totalPost, $sellerRating, $buyerRating, $duration) {

    echo '<tr>

    <td><a href="';
    generateUrlFromMemberId($id);

    echo '"><i class="icon-user"></i>' . ' ' . $memberName . '</a></td>
        <td style="text-align: center">' . $totalPost . '</td>
    <td style="text-align: center">' . $sellerRating . '</td>
        <td style="text-align: center">' . $buyerRating . '</td>
    <td style="text-align: center">' . $duration . '</td></tr>';
}
?>

<p>
<tt><?php //echo __FILE__;                             ?></tt>
</p>

<!-- end of viewMemberList -->


<!-- viewReportList --> 
<?php
/* @var $this MemberController */
$currentUrl = Yii::app()->request->url;

function showReport($id, $messsage, $Member_idMember, $dateReport, $postReport) {

    $name = Member::model()->findByPk($id);
    echo '<tr>

    <td><a href="viewMemberDetail?idMember=' . $id;
    echo '">' . $name->username . '</a></td>';
    if (isset($postReport)) {
        echo '<td style="text-align:center"><a href="../post/viewPostDetail?idPost=' . $postReport->idPost . '">' . $postReport->productName . '</td>';
    } else
        echo '<td style="text-align:center"> - </td>';
    echo '<td style="text-align:center">' . $messsage . '</td>
    <td style="text-align:center"><a href="viewMemberDetail?idMember=' . $Member_idMember->idMember . '">' . $Member_idMember->username . '</a></td>
    <td style="text-align:center">' . $dateReport . '</td></tr>';
}
?>

<p>
<tt><?php //echo __FILE__;                             ?></tt>
</p>

<!-- end of viewReportList -->



<div style="display: block;" >
    <?php if (Yii::app()->user->getState("isAdmin")) { ?>
        <ul class="nav nav-pills pull-right" id="tabMember">
            <li id="liMember" <?php if (!isset($resultApproval)) { ?>class="active" <?php } ?>>
                <a data-toggle="tab" href="#ContentMember">Members</a>
            </li>
            <li id="liPending" <?php if (isset($resultApproval)) { ?>class="active" <?php } ?>>
                <a data-toggle="tab" href="#ContentPending">Pending accounts&nbsp;<?php if (count($pendingAccounts) > 0) echo "<span class=\"label label-important\">" . count($pendingAccounts) . "</span>"; ?></a>
            </li>  
            <!--  user list has been repoted -->
            <li id="liReport" >
                <a data-toggle="tab" href="#ContentReport">Reported accounts&nbsp;<?php if (count($reportingAccounts) > 0) echo "<span class=\"label label-warning\">" . count($reportingAccounts) . "</span>"; ?></a>
            </li>  
        </ul>
    <?php } ?>
</div>
<div class="tabbable"> <!-- Only required for left/right tabs -->

    <!-- ************big pane*************** -->
    <div class="tab-content" style="overflow: visible">

        <!--  Member pane -->
        <div class="tab-pane <?php if (!isset($resultApproval)) { ?>active<?php } ?>" id="ContentMember">

            <div class="accordion-heading">
                <b class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" align="center">
                    <h1 align="left">Members</h1>
                </b>
            </div>

            <form class="form-search" method="GET">
                <div align="left" style="margin-left: 25px;">
                    <div class="input-append">
                        <input name="search"type="text" class="input-xlarge search-query" placeholder="Member's name" value="<?php echo (isset($_GET['search']))? $_GET['search'] : ""; ?>"/>
                        <button type="submit" class="btn"><i class="icon-search"></i></button>
                    </div>
                </div>
            </form>

            <div class="main_div" style="margin-top: 20px"> 
                <table class="table">
                    <thead>
                        <tr>
                            <th class="span4" bgcolor="#e74c3c" style="color:white">Member Name</th>
                    <th class="span1" bgcolor="#e74c3c" style="color:white"><center># posts</center></th>
                    <th class="span1" bgcolor="#e74c3c" style="color:white"><center>Selling rating</center></th>
                    <th class="span1" bgcolor="#e74c3c" style="color:white"><center>Buying rating</center></th>
                    <th class="span1" bgcolor="#e74c3c" style="color:white">
                    <center>Registration Date</center>
                    </th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($allUser as $user) {
                            $imf = new InfoMemFinder();
                            $ratingSale = $imf->getScore($user->idMember);
                            if (empty($ratingSale['s'])) {
                                $ratingSale['s'] = "-";
                            } else {
                                $ratingSale['s'] = number_format($ratingSale['s'], 2);
                            }
                            if (empty($ratingSale['b'])) {
                                $ratingSale['b'] = "-";
                            } else {
                                $ratingSale['b'] = number_format($ratingSale['b'], 2);
                            }
                            $postCount = $imf->getPostCount($user->idMember);
                            showMember($user->idMember, $user->username, $postCount, $ratingSale['s'], $ratingSale['b'], $user->registrationDate);
                        }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- End member pane -->

        <!-- /////////////////////////// admin pane ///////////////////////////// -->
        <?php if (Yii::app()->user->getState("isAdmin")) { ?>
            <!--  pending pane -->
            <div class="tab-pane <?php if (isset($resultApproval)) { ?>active<?php } ?>" id="ContentPending">
                <div class="main_div" style="margin-top: 20px">
                    <h2>Pending Accounts</h2>
                    <?php
                    if (isset($resultApproval)) {
                        if ($resultApproval['typeOfMsg'] == 1) {
                            ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Success!</strong>
                                <?php
                                echo $resultApproval['msg'];
                            } else {
                                ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Error!</strong>
                                    <?php
                                    echo $resultApproval['msg'];
                                }
                                ?>
                            </div>
                            <?php
                        } if (count($pendingAccounts) > 0) { /* ?>
                          <div class="pull-right">
                          <em><?php pendingAmount(count($pendingAccounts)); ?></em>
                          </div>
                          <?php */
                        }
                        if (count($pendingAccounts) > 0) {
                            ?>
                            <table class = "table">
                                <thead>
                                    <tr>
                                        <th class = "span1" bgcolor = "#e74c3c" style = "color:white">
                                <div align = "center" >ID</div>
                                </th>
                                <th class = "span3" bgcolor = "#e74c3c" style = "color:white">
                                <div id = "type">Account Name</div>
                                </th>
                                <th class = "span2" bgcolor = "#e74c3c" style = "color:white">Firstname</th>
                                <th class = "span2" bgcolor = "#e74c3c" style = "color:white">Lastname</th>

                                <th class = "span2" bgcolor = "#e74c3c" style = "color:white">
                                <div id = "type"align = "center" >E-mail</div>
                                </th>
                                <th class = "span2" bgcolor = "#e74c3c" style = "color:white">
                                <div id = "type"align = "center" >Tel. number</div>
                                </th>

                                <th class = "span3" bgcolor = "#e74c3c" style = "color:white">
                                <div id = "type"align = "center">Registration Date</div>
                                </th>
                                <th class = "span1" bgcolor = "#e74c3c" style = "color:white">
                                <div id = "type"align = "center" >Approval</div>
                                </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($pendingAccounts as $user)
                                        showPendingAccount($user->idMember, $user->username, $user->firstname, $user->lastname, $user->email, strTel($user->telephonenumber), $user->registrationDate);
                                    ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <center><h4><em>There are no requests pending for approval.</em></h4></center>
                        <?php } ?>
                    </div>
                </div>
                <!--  End pending pane -->

                <!--  Report pane -->
                <div class="tab-pane " id="ContentReport">

                    <div class="main_div" style="margin-top: 20px">
                        <h2>Reporting list</h2>


                        <?php if (count($reportingAccounts) > 0) { ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="span2" bgcolor="#e74c3c" style="color:white">Issue Member</th>
                                        <th class="span2" bgcolor="#e74c3c" style="color:white">
                                <div id="type"align="center" >Issue Post</div>
                                </th>
                                <th class="span5" bgcolor="#e74c3c" style="color:white">
                                <div id="type"align="center" >Report message</div>
                                </th>
                                <th class="span2" bgcolor="#e74c3c" style="color:white">
                                <div id="type"align="center" >Reporter</div>
                                </th>
                                <th class="span2" bgcolor="#e74c3c" style="color:white">
                                <div id="type"align="center" >Report time</div>
                                </th>               
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($reportingAccounts as $reportUser) {
                                        showReport($reportUser->referred_idMember, $reportUser->message, $reportUser->memberIdMember, $reportUser->dateReport, $reportUser->postIdPost);
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <center><h4><em>There are no reported accounts now.</em></h4></center>
                        <?php } ?>
                    </div>

                </div>
                <!--  End Report pane -->

                <!-- /////////////////////////// End admin pane ///////////////////////////// -->
                <?php
            }
            ?>

            <!--********End Big pane*********-->
        </div>
        <div align="right">
            <?php $this->widget('CLinkPager', array('pages' => $pages,)) ?>
            <br />
        </div>
    </div>
    <script>
        function hasClass(element, className) {
            return element.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(element.className);
        }
        //    $(".nav.nav-pills.pull-right li").on("click", function() {
        //        if (hasClass(document.getElementById('liPending'), "active")) {
        //            document.getElementById('ContentMember').style.display = "block";
        //            document.getElementById('ContentPending').style.display = "none";
        //
        //        } else if (hasClass(document.getElementById('liMember'), "active")) {
        //            document.getElementById('ContentMember').style.display = "none";
        //            document.getElementById('ContentPending').style.display = "block";
        //
        //        }
        //
        //    });
        $(".btnApprove").on("click", function() {
            this.innerHTML = "Approving";

        });

        $(".btnDecline").on("click", function() {
            this.innerHTML = "Processing";

        });
        $("#liPending").on("click", function() {
            document.getElementById('ContentMember').style.display = "none";
            document.getElementById('ContentPending').style.display = "block";
            document.getElementById('ContentReport').style.display = "none";
        });
        $("#liMember").on("click", function() {
            document.getElementById('ContentMember').style.display = "block";
            document.getElementById('ContentPending').style.display = "none";
            document.getElementById('ContentReport').style.display = "none";
        });
        $("#liReport").on("click", function() {
            document.getElementById('ContentMember').style.display = "none";
            document.getElementById('ContentPending').style.display = "none";
            document.getElementById('ContentReport').style.display = "block";
        });
    </script>