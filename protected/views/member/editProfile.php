<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name . ' - editProfile';

/*
  $this->breadcrumbs=array(
  'Site'=>array('/site'),
  'Register',
  ); */
?>


<?php /*
  <h1><?php echo $this->id . '/' . $this->action->id; ?></h1>
 */ ?>
<p>
<tt><?php //echo __FILE__;                   ?></tt>
</p>

<?php 
    if(isset($invalid) && $invalid == 1){
        $msg2 = "";
        $typeOfMsg2 = 0;
        if( isset($error->username) && $error->username == 1) $msg2 = $msg2 . " - Username";
        if( isset($error->password) && $error->password == 1) $msg2 = $msg2 . " - Password";
        if( isset($error->password) && $error->password == 1) $msg2 = $msg2 . " - Re-password";
        if( isset($error->firstname) && $error->firstname == 1) $msg2 = $msg2 . " - Firstname";
        if( isset($error->lastname) && $error->lastname == 1) $msg2 = $msg2 . " - Lastname";
        if( isset($error->citizenID) && $error->citizenID == 1) $msg2 = $msg2 . " - Citizen ID";
        if( isset($error->address) && $error->address == 1) $msg2 = $msg2 . " - Address";
        if( isset($error->country) && $error->country == 1) $msg2 = $msg2 . " - Country";
        if( isset($error->telephonenumber) && $error->telephonenumber == 1) $msg2 = $msg2 . " - Telephone number ";
        if( isset($error->email) && $error->email == 1) $msg2 = $msg2 . " - E-mail";
    }
?>

<div class="mainRegisterPage_div">
    <div class="form">
        <div class="head_RegisterPage">
            <h1>Edit Profile</h1>
            <p class="note">โปรดกรอกข้อมูลเพื่อแก้ไขข้อมูลสมาชิก</p>
        </div>
        <?php
        if (isset($msg)) {
            if ($typeOfMsg == 1) {
                ?>
                <div style="display: block;">
                    <div class="alert alert-success" style="margin: 15px 50px;">
                        <strong>Completed!</strong>&nbsp;<?php echo $msg; ?>
                    </div>
                </div>
            <?php } else { ?>
                <div style="display: block;">
                    <div class="alert alert-error" style="margin: 15px 50px;">
                        <strong>Error!</strong>&nbsp;<?php echo $msg; ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>

        <?php
        if (isset($msg2)) {
            if ($typeOfMsg2 == 0) { ?>
                <div style="display: block;">
                    <div class="alert alert-error" style="margin: 15px 50px;">
                        <strong>Error! </strong>ข้อมูลผิดพลาด โปรดกรอกข้อมูลใหม่อีกครั้ง<br><?php echo $msg2; ?>
                    </div>
                </div>
                <?php }
        }
        ?>
        <div style="display: block; height: 350px">
            <div class="column1_RegisterPage">
                <br>
                <form class="form-horizontal">
                    <div class="control-group success"><label class="control-label required" for="Member_username">Username <span class="required"></span></label><div class="controls"><input name="username" value="<?php echo $member->username; ?>" id="Member_username" type="text" maxlength="45" disabled><span class="help-inline error" id="Member_username_em_" style="display: none;"></span></div></div>
                    <div class="control-group success"><label class="control-label required" for="Member_password">Password <span class="required"></span></label><div class="controls"><a class="btn btn-warning" href="#changePassword" role="button" data-toggle="modal">Change password ...</a><span class="help-inline error" id="Member_password_em_" style="display: none;"></span></div></div>                
                </form>
                <!--div class="control-group success"><label class="control-label required" for="Member_password">Re-Password <span class="required">*</span></label><div class="controls"><input name="repassword" id="Member_repassword" type="password" maxlength="45"><span class="help-inline error" id="Member_password_em_" style="display: none;"></span></div></div>  -->              
            </div>
            <div id="changePassword" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordTitle" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="changePasswordLabel">Change password</h3>
                </div>
                <form class="form-horizontal" method="post" action="changePassword" style="margin: 0px">
                    <div class="modal-body">

                        <div class="control-group">
                            <label class="control-label" for="oldPassword">Old password</label>
                            <div class="controls">
                                <input type="password" id="oldPassword" name="oldPassword">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="newPassword">New password</label>
                            <div class="controls">
                                <input type="password" id="newPassword" name="newPassword">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="newPassword2">New password (re-type)</label>
                            <div class="controls">
                                <input type="password" id="newPassword2" name="newPassword2">
                            </div>
                        </div>
                        <div class="control-group"><div class="alert alert-info"><strong>Notice!</strong>&nbsp;Your password must be 6-20 characters length.</div></div>

                        <input type="hidden" name="idMember" value="<?php echo Yii::app()->user->getState("cID"); ?>">
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-warning btn-large" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
            <form method="POST" action="edit" class="form-horizontal">
                <div class="column2_RegisterPage">
                    <br>
                        <div class="control-group "><label class="control-label required" for="Member_firstname">First name <span class="required">*</span></label><div class="controls"><input class="" name="firstname" value="<?php echo $member->firstname; ?>" id="Member_firstname" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_firstname_em_" style="display: none"></span></div></div>                
                        <div class="control-group "><label class="control-label required" for="Member_lastname">Last name <span class="required">*</span></label><div class="controls"><input class="" name="lastname" value="<?php echo $member->lastname; ?>" id="Member_lastname" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_lastname_em_" style="display: none"></span></div></div>
                        <div class="control-group "><label class="control-label required" for="Member_address">Citizen ID <span class="required">*</span></label><div class="controls"><input class="" name="citizenID" value="<?php echo $member->citizenID; ?>" id="Member_citizenID" type="text" maxlength="13" required=""><span class="help-inline error" id="Member_citizenID_em_" style="display: none"></span></div></div>
                        <div class="control-group "><label class="control-label required" for="Member_address">Address <span class="required">*</span></label><div class="controls"><input class="span4" name="address" value="<?php echo $member->address; ?>" id="Member_address" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_address_em_" style="display: none"></span></div></div>
                        <div class="control-group "><label class="control-label required" for="Member_country">Country <span class="required">*</span></label><div class="controls"><input class="" name="country" value="<?php echo $member->country; ?>" id="Member_country" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_country_em_" style="display: none"></span></div></div>                
                        <div class="control-group "><label class="control-label required" for="Member_telephonenumber">Telephone number <span class="required">*</span></label><div class="controls"><input class="" name="telephonenumber" value="<?php echo $member->telephonenumber; ?>" id="Member_telephonenumber" type="text" maxlength="12" required=""><span class="help-inline error" id="Member_telephonenumber_em_" style="display: none"></span></div></div>                
                        <div class="control-group "><label class="control-label required" for="Member_email">E-mail <span class="required">*</span></label><div class="controls"><input class="" name="email" value="<?php echo $member->email; ?>" id="Member_email" type="text" maxlength="45" required=""><span class="help-inline error" id="Member_email_em_" style="display: none"></span></div></div>
                        <input type="hidden" name="idMember" value="<?php echo $member->idMember; ?>">
                        <input type="hidden" name="username" value="<?php echo $member->username; ?>">
                </div>
        </div>
        <div class="form-actions" style="display: block;">
            <center>
                <p align="center"><input class="btn btn-primary btn-large" type="submit" value="Save changes" name="B1"></p>
            </center>
        </div>

            </form>
    </div>
</div>