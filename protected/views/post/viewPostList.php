<?php
/* @var $this PurchasedController */

?>

<p>
    <tt><?php //echo __FILE__; ?></tt>
</p>

<?php 
function showPost($type, $status, $postName, $price, $date, $postId) {
    $price = number_format($price);
    if ($type == "ac" && $status == "Open") {
        echo '<td style="text-align:center"><a href="#" rel="tooltip" data-original-title="ราคาปัจจุบัน ' . $price
        . ' บาท, ปิดประมูลวันที่ 12/01/57 12:00 น."><span class="label label-important">Auction</span></td>';
    } else
    if ($type == "ds" && $status == "Open") {
        echo ' <td style="text-align:center">
        <a href="#" rel="tooltip" data-original-title="ราคา ' . $price . ' บาท"><span class="label label-important">Direct sale</span></a> 
        </td>';
    } else
    if ($type == "ds" && $status == "Purchased") {
        echo '<td style="text-align:center">
        <a style="color:gray" rel="tooltip" data-original-title="ราคาขาย ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
        </td>';
    } else
    if ($type == "ac" && $status == "Purchased") {
        echo '<td style="text-align:center">
        <a style="color:gray" rel="tooltip" data-original-title="ปิดประมูลที่ ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
        </td>';
    } else if ($status == "Canceled"){
        echo '<td style="text-align:center">
        <a style="color:gray" rel="tooltip" data-original-title="ถูกยกเลิก"><span class="label label-inverse">Canceled</span></a>
        </td>';
    }
    echo '<td><a href=';
    echo "viewPostDetail?idPost=" . $postId;
    echo '>' . $postName . '</td><td style="text-align:center">' . $date . '</td></tr>';
}

function showPurchasedPost($postId, $postName, $seller , $purchasedDate, $type, $status, $price) {
    $price = number_format($price);
    if ($type == "ds" && $status == "Purchased") {
        echo '<td style="text-align:center">
        <a style="color:gray" rel="tooltip" data-original-title="ราคาขาย ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
        </td>';
    } else
    if ($type == "ac" && $status == "Purchased") {
        echo '<td style="text-align:center">
        <a style="color:gray" rel="tooltip" data-original-title="ปิดประมูลที่ ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
        </td>';
    } else if ($status == "Canceled"){
        echo '<td style="text-align:center">
        <a style="color:gray" rel="tooltip" data-original-title="ถูกยกเลิก"><span class="label label-inverse">Canceled</span></a>
        </td>';
    }
    else {
        echo '<td></td>';
    }
    echo '<td><a href=';
    $url = "viewPostDetail?idPost=" . $postId;
    echo $url;
    echo '>' . $postName . '</td>
    <td style="text-align:center">' . $seller . '</td><td style="text-align:center">' . $purchasedDate . '</td><tr>';
}

?>
<h2><?php echo (Yii::app()->user->getState('cID') == $memberProfile->idMember)? "My " : $memberProfile->username . "'s "; ?>posts</h2>
<hr />
<div class="accordion" id="accordion2" style="clear: both">
    <div class="accordion-group">
        <div class="accordion-heading">
            <b class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                <h3><?php if(Yii::app()->user->getState('cID') == $memberProfile->idMember)echo "My Post"; else echo $memberProfile->username."'s Post";?><?php echo ' ('.$countPost.')';?></h3>
            </b>
        </div>

        <?php if($countPost!=0){?>
        <div id="collapseOne" class="accordion-body collapse <?php if(isset($_GET['page'])) echo 'in';?>">
            <div class="accordion-inner">
                <div style="width:80%; margin-left:10%">
                    <!-- PostList -->
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="span1" bgcolor="#e74c3c" style="color:white">
                                    <div align="center" >Type</div>
                                </th>
                                <th class="span5" bgcolor="#e74c3c" style="color:white">Post Name</th>

                                <th class="span2" bgcolor="#e74c3c" style="color:white">
                                    <div id="type"align="center">Date</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($memberPost as $post) {
                                showPost($post->type, $post->status, $post->productName, $post->price, $post->postDateTime, $post->idPost);
                            }
                            ?>

                        </tbody>
                    </table>
                    <div align="right">
                        <?php $this->widget('CLinkPager', array('pages' => $pages,))?>
                        <br />
                    </div>
                </div>
            </div>
        </div>


        <?php } ?>
    </div>

    <div class="accordion-group">
        <div class="accordion-heading">
            <b class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                <h3><?php if(Yii::app()->user->getState('cID') == $memberProfile->idMember)echo "My Purchased"; else echo $memberProfile->username."'s Purchased";?><?php echo ' ('.$countPurchasedPost.')';?></h3>
            </b>
        </div>
        <?php if($countPurchasedPost!=0){?>
        <div id="collapseTwo" class="accordion-body collapse">
            <div class="accordion-inner">
                <div class="tab-pane" id="myPurchased">
                    <div class="mainPurchasedList_div">
                        <p>
                            <!-- PostList -->
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="span1" bgcolor="#27ae60" style="color:white">
                                            <div align="center" >Status</div>
                                        </th>
                                        <th class="span4" bgcolor="#27ae60" style="color:white">Post Name</th>
                                        <th class="span2" bgcolor="#27ae60" style="color:white">
                                            <div id="type"align="center" >Seller</div>
                                        </th>
                                        <th class="span2" bgcolor="#27ae60" style="color:white">
                                            <div id="type"align="center">Purchased Date</div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    foreach ($memberPurchasedPost as $post) {
                                        showPurchasedPost($post->idPost, $post->productName, Member::model()->findByPk($post->Seller_Member_idMember)->username, $post->postDateTime, $post->type, $post->status, $post->price);
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div align="right">
                                <?php //$this->widget('CLinkPager', array('pages' => $pages_purchased,))?>
                                <br />
                            </div>
                        </p> 
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

