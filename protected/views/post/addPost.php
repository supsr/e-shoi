<?php ?>
<div id="addPostHead"><h2>Add post</h2></div><br><br><br>
<div    class="span8" id="addPostBody">
    <div style="margin-left:120px;">
    <form class="form-horizontal" method="POST" action="addPostProcess" enctype="multipart/form-data">
        <input type="hidden" value="<?php echo $seller_Member_idMember ?>" name="Member_idMember" </p>                                  
        <div class="control-group">
            <label class="control-label" for="inputProductName">Product name</label>
            <div class="controls">
                <input class="input-xlarge" type="text" id="inputProductName" name="productName" placeholder="Product name" size="20" required="">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputProductDetail">Product detail</label>
            <div class="controls">
                <textarea id="inputProductDetail" name="productDetail" placeholder="Product detail" size="30" rows="4" style="width: 20em"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputProductImage">Image URL</label>
            <div class="controls">
                <input type="file" name="productImage" id="inputProductImage" accept="image/*">
            </div>
        </div>
        <div class="control-group" style="display: none">
            <label class="control-label" for="inputProductStatus">Status</label>
            <div class="controls">
                <select size="1" name="status" id="inputProductStatus">
                    <option selected>Open</option>
                    <option>Purchased</option>
                    <option>Canceled</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="type">Type</label>
            <div class="controls">
                <select size="1" name="type" id='type'>
                    <option value="ds" selected>Direct Sale</option>
                    <option value="ac">Auction</option>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" id="labelInitialPrice" for="inputInitialPrice">Price</label>
            <div class="controls">
                <div class="input-prepend input-append">
                    <span class="add-on">฿</span>
                    <input id="inputInitialPrice" class="input-medium" type="text" name="price" size="20" style="text-align: right" value="1" placeholder="Initial price" required>
                    <span class="add-on">.00</span>
                </div>
            </div>
        </div>

        <div id="ac_part" style="display: none">
            <div class="control-group">
                <label class="control-label" for="inputMinimumBidRate">Minimum bidding price</label>
                <div class="controls">
                    <div class="input-prepend input-append">
                        <span class="add-on">฿</span>
                        <input id="inputMinimumBidRate" class="input-medium" type="text" name="minimumBidRate" size="20" style="text-align: right" value="1" placeholder="Minimum bidding price">
                        <span class="add-on">.00</span>
                    </div>
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="inputCloseDateTime">Auction ending time</label>
                <div class="controls">

                        <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
                            <link rel="stylesheet" type="text/css" media="screen"
                             href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">

                             <div id="datetimepicker" class="input-append date">
                                  <input data-format="yyyy-MM-dd hh:mm:ss" id="inputCloseDateTime"  type="text" name="closeDateTime"  ></input>
                                  <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                  </span>
                                </div>
                                <script type="text/javascript"
                                 src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
                                </script> 
                                <script type="text/javascript"
                                 src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
                                </script>
                                <script type="text/javascript"
                                 src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
                                </script>
                                <script type="text/javascript"
                                 src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
                                </script>
                                <script type="text/javascript">
                                  $('#datetimepicker').datetimepicker({
                                  //  format: 'yyyy-MM-dd hh:mm:ss',
                                    language: 'en-us'
                                  });
                                </script>
               <? //    <input id="inputCloseDateTime"  type="text" name="closeDateTime" size="30" value="" placeholder="Please choose from the button" disabled>
                    // <a class="btn" href="#">Choose date&time ...</a>  ?>
                </div>
            </div>
        </div>


        <div class="control-group">
            <div class="controls">
                <input class="btn btn-large btn-primary" type="submit" value="Add post" name="B1">
<!--                <input class="btn" type="reset" value="Reset" name="B2">-->
            </div>
        </div>

    </form>
    <div>
<!--    <a href="post" ><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3_
                                                 HQdxK9E6yqM-UJxUZGYlZdPEXaSpnGXg1UsKbFJGvGdJJQN" class="img-circle" width="50px" height="50px"></a>-->
</div>
</div>



<script language="JavaScript">
    $('#type').on("change", function() {
        var selected_value = $("#type option:selected").val();
        if (selected_value == "ac") {
            fncShow('ac_part');
            document.getElementById('inputInitialPrice').placeholder = 'Initial Price';
            document.getElementById('labelInitialPrice').innerHTML = "Initial Price";
            document.getElementById('inputMinimumBidRate').required = true;
            document.getElementById('inputCloseDateTime').required = true;
        }
        if (selected_value == "ds") {
            fncHide('ac_part');
            document.getElementById('inputInitialPrice').placeholder = 'Price';
            document.getElementById('labelInitialPrice').innerHTML = "Price";
            document.getElementById('inputMinimumBidRate').required = false;
            document.getElementById('inputCloseDateTime').required = false;
        }
    });

    function fncShow(ctrl) {
        document.getElementById(ctrl).style.display = '';
    }

    function fncHide(ctrl) {
        document.getElementById(ctrl).style.display = 'none';
    }
</script>
