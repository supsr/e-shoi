<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<p>
<tt><?php //echo __FILE__;                     ?></tt>
</p>

<?php

function showBuyStage($post) {
    $invoice = Invoice::model()->findByPk($_GET['idPost']);
    echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px">

                        <h3><p>Buy Process</p></h3>

                        <div class="progress">
                            <div class="bar" style="width: ';
    if ($invoice->status == "Wait for Paying")
        echo '31';
    if ($invoice->status == "Wait for Product")
        echo '65';
    if ($invoice->status == "Complete")
        echo '99';

    echo '%;"></div>
                        </div>         

                        <p style="color:brown;font-family:Helvetica">BUY -----------------------------------------  WAIT FOR PAYING-----------------------------------------  WAIT FOR PRODUCT-----------------------------------------  COMPLETE</p>
                        <br>';

    if (Yii::app()->user->getState('cID') == $post->Buyer_Member_idMember && $invoice->status == "Wait for Product") {
        echo '<h4><p>Please give feetback (from buyer to seller) if you already get product.</p></h4>';
        showFeedBuyer($post);
    } else if (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Paying") {
        echo '<h4><p>Please give feetback (from seller to buyer)  if you already get payment.</p></h4>';
        showFeedSeller($post);
        echo '<div class="span9" ><div class="span6" ><h4><p>If you want to cancel this purchase (post will be close) ->></p></h4></div>';
        echo '<div class="span2" ><a class="btn btn-danger btn-large" href="cancelPost?idPost=' . $post->idPost . '"><i class="icon-pencil"></i>&nbsp;Cancel</a></div></div>';
    } else if (Yii::app()->user->getState('cID') == $post->Buyer_Member_idMember && $invoice->status == "Wait for Paying") {
        echo '<h4><p>*Please do payment and wait for seller to confirm payment.</p></h4>';
    } else if (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Product") {
        echo '<h4><p>*When buyer get product and return feedback this post will be complete.</p></h4>';
    } else if ($invoice->status == "Complete") {
        echo '<h3><p>Your trading successful !</p></h3>';
    }

    echo '</div>';
}

function showFeedBuyer($post) {
    echo '
                            <div class="span5">     
                                <form method="POST" action="/feedback/feedbackfrombuyer">   
                                    Score : 
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios1" value="0">
                                      0
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios2" value="1">
                                      1
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios3" value="2">
                                      2
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios4" value="3">
                                      3
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios5" value="4">
                                      4
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios6" value="5" checked>
                                      5
                                    </label>

                                    <input type="hidden" name="idPost" value="' . $post['idPost'] . '"/>               
                                    <div class="input-prepend input-append">
                                        <div class="btn-group">
                                            <span class="btn" disabled><i class="icon-user"></i>Buyer</span>
                                        </div>
                                        <input style="width: 320px;" name="feedbackfrombuyer" type="text" placeholder="buyer\'s feedback">
                                        <div class="btn-group">
                                            <input class="btn btn-info btn" type="submit" value="send">
                                        </div>
                                    </div>
                                </form>
                            </div>';
}

function showFeedSeller($post) {
    echo '   
                            <div class="span5">
                                <form method="POST" action="/feedback/feedbackfromseller">  
                                    Score : 
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="0">
                                      0
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="1">
                                      1
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="2">
                                      2
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="3">
                                      3
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="4">
                                      4
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="5" checked>
                                      5
                                    </label> 

                                    <input type="hidden" name="idPost" value="' . $post['idPost'] . '"/>                               
                                    <div class="input-prepend input-append">
                                        <div class="btn-group">
                                            <span class="btn" disabled><i class="icon-user"></i>Seller</span>
                                        </div>
                                        <input style="width: 320px;" name="feedbackfromseller" type="text" placeholder="seller\'s feedback">
                                        <div class="btn-group">
                                            <input class="btn btn-info btn" type="submit" value="send ">
                                        </div>
                                    </div>
                                </form>
                            </div>';
}

function generateUrlByPostId($postId) {
    $url = "viewPostDetail?idPost=" . $postId;
    echo $url;
}
function findImg($postID){
    $img = Post::model()->findByPk($postID)->productImage;
    
    $filename = Yii::app()->baseUrl . "/clients_upload/" . $img;
    if(empty($img)) return Yii::app()->baseUrl . "/clients_upload/null.JPG";
    else 
        return $filename;

}
function showPostPreview($type, $status, $postName, $price, $seller, $detail, $postId, $postDate) {
    $price = number_format($price);
    $name = Member::model()->findByPk($seller);
    echo '<div class="span4" style="background-color: #e6e6e6; border-radius: 5px; position: relative">
        <a class="btn btn-info pull-right disabled" style="position: absolute; right: 0px; bottom: 0; margin: 5px">฿ ' . $price . '</a>
                    <div style="position: absolute; top: 1px; left: 3px">';
    if ($status == "Open") {
        if ($type == "ac")
            echo '<span class="label label-important">Auction';
        // else echo '<span class="label label-important">Direct sale';
    } if ($status == "Purchased") {
        echo '<span class="label label-inverse">Purchased';
    }
    echo '</span>';
    echo '</div>
                    <a class="pull-left">
                        <img class="img-polaroid" src="' . findImg($postId) .'" style="width: 100px; height: 100px; margin: 7.5px;">
                    </a>

                    <div class="row" style="top: 0;  position: absolute; font-size: small; margin-bottom: 5px; width: 100%">
                        <div class="span4" style="width: 100%; text-align: justify">
                            <div style="position: absolute; margin-left: 130px; width: 230px; text-align: justify">
                            <a style="color: black" href="';
    echo generateUrlByPostId($postId);   
    echo '"><h4>' . $postName . '</h4></a>
                            <span>' . substr($detail, 0, 200) . '</span></div>
                        </div>
                    </div>

                    <div class="row" style="bottom: 0;  position: absolute; font-size: small; margin-bottom: 5px;">
                        <div class="span4" style="padding-left: 125px; width: 100%; position: relative">
                            
                                <span style="position: relative; right: 0px; bottom: 0px; font-size: small;">
                                <i class="icon-user"></i>&nbsp;<a href="member/member/viewMemberDetail?idMember=' . $name->idMember .'">' . $name->username . '</a> <span style="font-size: xx-small">(' . $postDate .')</span><br/>
                            </span>
                            
                        </div>
                    </div>

                </div>';
}

function showPost($type, $status, $postName, $price, $seller, $date, $postId) {
    $price = number_format($price);
    $name = Member::model()->findByPk($seller);

    if ($type == "ac" && $status == "Open") {
        echo '<td style="text-align:center"><a href="#" rel="tooltip" data-original-title="ราคาประมูลปัจจุบัน ' . $price
        . ' บาท"><span class="label label-important">Auction</span></td>';
    } else
    if ($type == "ds" && $status == "Open") {
        echo ' <td style="text-align:center">
                          <!--<a href="#" rel="tooltip" data-original-title="ราคา ' . $price . ' บาท"><span class="label label-important">Direct sale</span></a>--> 
                    </td>';
    } else
    if ($type == "ds" && $status == "Purchased") {
        echo '<td style="text-align:center">
                         <a style="color:gray" rel="tooltip" data-original-title="ราคาขาย ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
                    </td>';
    } else
    if ($type == "ac" && $status == "Purchased") {
        echo '<td style="text-align:center">
                        <a style="color:gray" rel="tooltip" data-original-title="ปิดประมูลที่ ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
                    </td>';
    }
    echo '<td><a href=';
    generateUrlByPostId($postId);
    echo '>' . $postName . '</td>
                    <td style="text-align:center">' . $name->username . '</td>
                    <td style="text-align:center">' . $date . '</td><tr>';
}
?>


<div class="main_div" style="width: 100%; margin-left: 0px">
    <?php
    if (isset($_GET['reportPost']) && $_GET['reportPost'] == 1) {
        ?>
        <div style="display: block;">
            <div class="alert alert-success" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Thank you for report problem. </strong><br>เจ้าหน้าที่จะทำการตรวจสอบและแก้ปัญหาให้โดยทันที<br>
            </div>
        </div>
<?php } else if (isset($_GET['reportPost']) && $_GET['reportPost'] == 0) { ?>
        <div style="display: block;">
            <div class="alert alert-error" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Report Fail!</strong><br>โปรดกรอกรายละเอียดของปัญหาที่จะร้องเรียนอีกครั้ง<br>
            </div>
        </div>

    <?php } ?>


    <?php
    if (!isset($allPost)) { //if allpostopen is null
        echo '<div class="span11" style="border:5px solid rgb(200,200,200);border-radius:20px;padding:15px;position:relative;margin-top:20px">';
        echo '<br><div class="span5">';
        if (strlen($post['productImage']) > 9)
            echo '<img  src="' . $post['productImage'] . '">';
        else if (isset($post['productImage']))
            echo '<img  src="' . Yii::app()->baseUrl . "/" . Settings::model()->findByPk("IMG_DIR")->Value . "/" . $post['productImage'] . '">';


        echo '<h3><a href="index" ><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3_HQdxK9E
                            6yqM-UJxUZGYlZdPEXaSpnGXg1UsKbFJGvGdJJQN" class="img-circle" width="70px" height="70px">Back to post list.</a></h3>
                            </div>'; //image post and back buttom

        echo '<div class="span5">';
        echo '<div id="status">Status:' . $post['status'] . '</div>'; //status box
        echo '<p align="center" style="font-family:cursive;font-size:28px;background-color:rgb(255,100,100);color:rgb(255,255,255);padding:10px"> ' . $post['productName'];
        echo '<hr width="450px"><p>  ' . $post['productDetail'];
        ?>
        <div class="main_div">

            <div class="column1_div">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <?php
                        if (isset($sellerUsername))
                            echo '<tr><td><h4>Seller : <a href="../member/viewMemberDetail?idMember=' . $sellerID . '">' . $sellerUsername . '</a></td></tr>';
                        if (isset($buyerUsername) && $post['status'] == "Purchased")
                            echo '<tr><td><h4>Buyer : <a href="../member/viewMemberDetail?idMember=' . $sellerID . '">' . $buyerUsername . '</a></td></tr>';
                        echo '<tr><td><h4>Price : ' . $post['price'] . '฿</h4></td></tr>';
                        echo '<div class="span2" style="margin:0px">';
                        if (Yii::app()->user->getState('cID') == $sellerID) { //EDITTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
                            echo '<tr><td><a class="btn btn-info btn-large" href="editPost?idPost=' . $post->idPost . '"><i class="icon-pencil"></i>&nbsp;Edit my post</a></td></tr>';
                        }
                        if (isset($post['buyerMemberIdMember']) && $post['type'] == "ds" && $post['status'] == "Purchased")
                            echo '<tr><td><div class="span3" style="margin:20px">
        <a href="../invoice/viewInvoice?postID=' . $post->idPost . '"><img width="80" height="80" src="https://cdn3.iconfinder.com/data/icons/rounded-monosign/142/invoice-512.png"></a></td></tr>';
                        if ($post['type'] == 'ac')
                            echo ' <tr><td><h4>Close bid date </h4> <i style="color:red">' . $auction['closeDateTime'] . '</i></td></tr> ';
                        ?>                          
                    </tbody>
                </table>

    <?php if (isset($sellerID) && $sellerID != Yii::app()->user->getState("cID")) { ?>
                    <div class="controls">
                        <a href="#report" role="button" data-toggle="modal">Report this post</a>
                    </div>
                    <div id="report" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordTitle" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3>Report this post</h3>
                        </div>
                        <form class="form-horizontal" method="post" action="reportpost">
                            <div class="modal-body">
                                <div class="control-group">
                                    <label class="control-label required" for="Member_username">ร้องเรียน</label>
                                    <div class="controls">
                                        <input name="username" disabled="disabled" value="<?php echo $post['productName'] . " (" . $sellerUsername . ")"; ?>" type="text">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">รายละเอียด</label>
                                    <div class="controls">
                                        <textarea rows="5" type="text" name="message"></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="referred_idMember" value="<?php echo $sellerID; ?>">
                                <input type="hidden" name="Post_idPost" value="<?php echo $post->idPost; ?>">
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                <button class="btn btn-primary" type="submit">Report</button>
                            </div>
                        </form>
                    </div>
    <?php } ?>

            </div>    

            <?php
            echo '</div><div class="span3" style="margin:0px">';
            if (!isset($post['buyerMemberIdMember']) && $post['type'] == "ds" && Yii::app()->user->getState('cID') != $sellerID && Yii::app()->user->getState('cID') != "")
//if this post not sell. show buy bottom  BUYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY BOTTOM
                echo '<a href="Buy?idPost=' . $post->idPost . '"><img src="http://www.magnetguncaddy.com/images/Buy-Now_ShoppingIcon.png"></a>';

            if (isset($post['Buyer_Member_idMember']) && $post['type'] == "ac" && Yii::app()->user->getState('cID') == $post['Buyer_Member_idMember']) {
                $invoice = Invoice::model()->findByPk($_GET['idPost']);
                if (!isset($invoice))
//if this post not sell. show buy bottom  BUYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY BOTTOM
                    echo '<a href="Buy?idPost=' . $post->idPost . '"><img src="http://www.magnetguncaddy.com/images/Buy-Now_ShoppingIcon.png"></a>';
            }

            echo '</div></div>';

            $feed = Feedback::model()->findByPk($post['idPost']);
            if (isset($feed)) {
                echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px">';

                if ($feed->feedbackFromBuyer != NULL) {
                    echo '<h4><p>Feedback from buyer to seller :</p></h4>' . $feed->feedbackFromBuyer;
                }

                if ($feed->feedbackFromSeller != NULL) {
                    echo '<h4><p>Feedback from seller to buyer :</p></h4>' . $feed->feedbackFromSeller;
                }

                echo '</div>';
            }


            if ($post->status == 'Purchased' && isset($_GET['idPost'])) {
                if (Yii::app()->user->getState('cID') == $post->Buyer_Member_idMember || Yii::app()->user->getState('cID') == $post->Seller_Member_idMember) {
                    showBuyStage($post);  //PROCESS OF BUY -------------------------------------
                }
            }




            if ($post['type'] == 'ac') { //auction show this table
                if ($post['status'] == "Open" && Yii::app()->user->getState('cID') != $sellerID && Yii::app()->user->getState('cID') != "") {
                    if (isset($error))
                        echo '<p style="color:red" class="span5">**** Bidding price must be more than mininum rate.****</p>';
                    echo '<div class="span10" style="margin-top:10px;border:2px solid rgb(255,200,200);border-radius:20px;padding:15px"><table width="100%" align="center">
    <tr>
        <td>
                <p align="center" style="color:rgb(255,255,255);background-color:rgb(255,100,100);padding:5px">Current Price: ' . $auction['currentPrice'] . '฿</p>
                <h3><p align="center">Minimum Bid Rate ' . $auction['minimumBidRate'] . '฿</p></h3>
                    <form method="POST" action="bid">
            <p align="center">Bid more : <input type="text" name="bid" size="20"></p>
                        <input type="hidden" value="' . Yii::app()->user->getState('cID') . '" name="idMember">
                        <input type="hidden" value="' . $post['idPost'] . '" name="idPost">           
            <p align="center"><input type="submit" value="Submit" name="Bid"><input type="reset" value="Reset" name="B4"></p>
                    </form>
                </td>
                <td><form method="POST" action="autobid">
            <p align="center" style="color:rgb(255,255,255);background-color:rgb(255,100,100);padding:5px">Autobid</p>
                        <h3><p align="center">your autobid now: ';
                    if (isset($autobid['autobidRate'])) {
                        echo '' . $autobid['autobidRate'] . '';
                    } else {
                        echo ' - ';
                    }

                    echo '</h3>
                        <p align="center">Highest price : <input type="text" name="autobid" size="20"></p>
                        <input type="hidden" value="' . Yii::app()->user->getState('cID') . '" name="idMember">
                        <input type="hidden" value="' . $post['idPost'] . '" name="idPost">           
            <p align="center"><input type="submit" value="Submit" name="Bid"><input type="reset" value="Reset" name="B4"></p>
        </form>
                </td>
    </tr>
</table></div>';
                }

                echo '<div class="span8" style="margin:30px;padding-left:100px"><table  width="100%" align="center"> 
    <tr style="background-color:rgb(255,100,105);color:rgb(255,255,255)" align="center">
        <td><h4>Date</h4></td>
        <td><h4>Name</h4></td>
        <td><h4>Price</h4></td>
                <td><h4>Autobid</h4></td>
    </tr>';
                foreach ($allBid as $bid) { //show bid challenge table
                    echo '<tr style="background-color:rgb(250,240,255)" align="center"><td>' . date("Y-m-d H:i:s", strtotime($bid['timestamp'])) . '</td><td>' . $bid['buyerUsername'] . '</td><td>' . $bid['price'] . '</td>';
                    if ($bid['autobid']) {
                        echo '<td>Yes</td>';
                    } else {
                        echo '<td></td>';
                    }
                    echo '</tr>';
                }

                echo '</table></div></div>';
            }
        } else {
            ?>

            <?php
            $this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
                'heading' => CHtml::encode(Yii::app()->name)));
            $this->endWidget();
            ?>  

            <div class="pull-left">
                <!-- AddPost Button -->
                <?php
                if (!Yii::app()->user->isGuest) {
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'label' => '<i class="icon-plus-sign icon-white"></i> Add post',
                        'type' => 'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                        'url' => array('/post/addPost'),
                        'encodeLabel' => false,
                        'size' => 'large',
                    ));
                }
                ?>
            </div>
            <form class="form-search" method="GET">
                <div align="center">
                    <div class="input-append">

                        <input name="search" type="text" placeholder="Type keyword here" class="input-xlarge search-query">
                        <button type="submit" class="btn"><i class="icon-search"></i></button>
                    </div>
                </div>
            </form>

            <!-- PostList -->

            <br />
            <?php
            for ($i = 0; $i < count($allPost); $i++) {
                if ($i % 3 == 0) echo "<div class=\"row\" style=\"margin-bottom: 10px\">";
                showPostPreview($allPost[$i]->type, $allPost[$i]->status, $allPost[$i]->productName, $allPost[$i]->price, $allPost[$i]->Seller_Member_idMember, $allPost[$i]->productDetail, $allPost[$i]->idPost, date_format(date_create($allPost[$i]->postDateTime), 'd/m/Y'));
                if ($i % 3 == 2 || $i == count($allPost)) echo "</div>";
            }
            ?>

                <?php array('label' => 'Contact', 'url' => array('/site/contact')); ?>

            <div align="right" style="clear: both; margin-top: 10px">
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                ))
                ?>
                <br />
            </div>
<?php } ?>
    </div>

    <style type="text/css">

        #status{
            padding: 20px;
            background-color: rgb(245,240,250);
            width: 200px;
            position: absolute;
            border-radius: 200px;
            border: 5px solid rgb(255,155,55);
            top: -40px;
            right: 900px;
            font-family: cursive;
            font-size: 24px;
            text-align: center;
            color: rgb(255,10,100)
        }

    </style>

    <?php /*
      $gridDataProvider = new CArrayDataProvider(array(
      array('id'=>1, 'type'=>'Auction', 'postName'=>'Mark', 'by'=>'Otto', 'date'=>'CSS'),
      array('id'=>2, 'type'=>'DirectSale', 'postName'=>'Jacob', 'by'=>'Thornton', 'date'=>'JavaScript'),
      array('id'=>3, 'type'=>'Purchased', 'postName'=>'Stu', 'by'=>'Dent', 'date'=>'HTML'),
      ));

      $this->widget('bootstrap.widgets.TbGridView', array(
      'dataProvider'=>$gridDataProvider,
      'template'=>"{items}",
      'columns'=>array(
      array('name'=>'type', 'header'=>'Type'),
      array('name'=>'postName', 'header'=>'Post Name'),
      array('name'=>'by', 'header'=>'Seller'),
      array('name'=>'date', 'header'=>'Date'),

      ),
      ));


      <p>You may change the content of this page by modifying the following two files:</p>

      <ul>
      <li>View file: <code><?php echo __FILE__; ?></code></li>
      <li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
      </ul>

      <p>For more details on how to further develop this application, please read
      the <a href="http://www.yiiframework.com/doc/">documentation</a>.
      Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
      should you have any questions.</p>

     */
    ?>
