<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<p>
<tt><?php //echo __FILE__;                              ?></tt>
</p>

<?php

function showBuyStage($post) {
    $invoice = Invoice::model()->findByPk($_GET['idPost']);
    echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px; border-radius: 5px">

                        <h3><p>Trading process</p></h3>';

    if ($invoice->status == "Wait for Paying")
        echo '<div class="progress progress-striped active" style="margin: 15px"><div class="bar" style="width: 33%"></div>';
    if ($invoice->status == "Wait for Product")
        echo '<div class="progress progress-striped active" style="margin: 15px"><div class="bar" style="width: 66%"></div>';
    if ($invoice->status == "Complete")
        echo '<div class="progress progress-success active" style="margin: 15px"><div class="bar" style="width: 100%"></div>';
    echo '</div>';

    $currentStatus = $invoice->status;
    $s_completed = '<span class="badge" style="margin-left: 18px"';
    $s_wfProduct = '<span class="badge" style="margin-left: 18px"';
    $s_wfPaying = '<span class="badge" style="margin-left: 18px"';
    $s_buy = '<span class="badge" style="margin-left: 18px"';
    switch ($currentStatus) {
        case 'Wait for Paying': {
                $s_wfPaying = '<i class="icon-hand-right"></i>&nbsp;<span class="badge badge-info"';
                $s_buy = '<i class="icon-ok"></i>&nbsp;<span class="badge badge-success"';
                break;
            }
        case 'Wait for Product': {
                $s_wfProduct = '<i class="icon-hand-right"></i>&nbsp;<span class="badge badge-info"';
                $s_wfPaying = '<i class="icon-ok"></i>&nbsp;<span class="badge badge-success"';
                $s_buy = '<i class="icon-ok"></i>&nbsp;<span class="badge badge-success"';
                break;
            }
        case 'Complete': {
                $s_completed = '<i class="icon-hand-right"></i>&nbsp;<span class="badge badge-success"';
                $s_wfProduct = '<i class="icon-ok"></i>&nbsp;<span class="badge badge-success"';
                $s_wfPaying = '<i class="icon-ok"></i>&nbsp;<span class="badge badge-success"';
                $s_buy = '<i class="icon-ok"></i>&nbsp;<span class="badge badge-success"';
                break;
            }

        default:
            $s_buy = '<i class="icon-hand-right"></i>&nbsp;<span class="badge badge-info"';
    }
    echo '<div class="span10">'
    . $s_buy . '>1.</span> Buy<br />'
    . $s_wfPaying . '>2.</span> Wait for paying<br />'
    . $s_wfProduct . '>3.</span> Wait for product<br />'
    . $s_completed . '>4.</span> Completed'
    . '</div></div><br>';
    if (Yii::app()->user->getState('cID') == $post->Buyer_Member_idMember && $invoice->status == "Wait for Product") {
        echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px; border-radius: 5px">';
    
        echo '<table style="width: 100%"><tr><td style="width: 65%; ' . ( (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Paying") ? 'border-right: 1px dashed rgb(180,180,180)">' : '">');
    
        echo '<h4><p>Please give a feedback to seller if you have already gotten product</p></h4>';
        showFeedBuyer($post);
        echo '</td></tr></table>';
        echo '</div>';
    } else if (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Paying") {
        echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px; border-radius: 5px">';
    
        echo '<table style="width: 100%"><tr><td style="width: 65%; ' . ( (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Paying") ? 'border-right: 1px dashed rgb(180,180,180)">' : '">');
    
        echo '<h4><p>Please give a feedback to buyer if you have already done the payment</p></h4>';
        showFeedSeller($post);
        echo '</td><td style="width: 40%; text-align: center;"><div style="width: 100%;">Click the below button to cancel this purchase<br /><br />';
        echo '<div style="display: block; text-align: center"><a class="btn btn-danger btn-large" href="cancelPost?idPost=' . $post->idPost . '"><i class="icon-remove icon-white"></i>&nbsp;Cancel purchase</a></div></div></td></tr></table>';
        echo '</div>';
    } else if (Yii::app()->user->getState('cID') == $post->Buyer_Member_idMember && $invoice->status == "Wait for Paying") {
        echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px; border-radius: 5px">';
    
        echo '<table style="width: 100%"><tr><td style="width: 65%; ' . ( (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Paying") ? 'border-right: 1px dashed rgb(180,180,180)">' : '">');
    
        echo '<h4><p>* Please do the payment and wait for seller to confirm his receipt</p></h4>';
        echo '</td></tr></table>';
    } else if (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Product") {
        echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px; border-radius: 5px">';
    
        echo '<table style="width: 100%"><tr><td style="width: 65%; ' . ( (Yii::app()->user->getState('cID') == $post->Seller_Member_idMember && $invoice->status == "Wait for Paying") ? 'border-right: 1px dashed rgb(180,180,180)">' : '">');
    
        echo '<h4><p>* When buyer get product and return feedback this post will be complete.</p></h4>';
        echo '</td></tr></table>';
        echo '</div>';
    } else if ($invoice->status == "Complete") {
    }
    
}

function showFeedBuyer($post) {
    echo '
                            <div style="width: 100%; margin-top:20px">
                                <form  class="form-horizontal" method="POST" action="../feedback/feedbackfrombuyer">  
                                    <div class="control-group">
                                    <label class="control-label">
                                    Score : 
                                    </label>
                                    <div class="controls">
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios1" value="0">
                                      0
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios2" value="1">
                                      1
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios3" value="2">
                                      2
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios4" value="3">
                                      3
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios5" value="4">
                                      4
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefrombuyer" id="optionsRadios6" value="5" checked>
                                      5
                                    </label>
                                    </div>
                                    <input type="hidden" name="idPost" value="' . $post['idPost'] . '"/>
                                    <br />    
                                    <div class="input-prepend input-append" style="display: block;">
                                        <span class="add-on" disabled><i class="icon-user"></i> Buyer</span>&nbsp;
                                        
                                        <input style="width: 320px;" name="feedbackfrombuyer" id="appendedPrependedInput" type="text" placeholder="Feedbacks to seller">
                                        <div class="btn-group">
                                            <input class="btn btn-info btn" type="submit" value="Send">
                                        </div>
                                    </div>
                                </form>
                            </div>';
}

function showFeedSeller($post) {
    echo '   
                            <div style="width: 100%; margin-top:20px">
                                <form  class="form-horizontal" method="POST" action="../feedback/feedbackfromseller">  
                                    <div class="control-group">
                                    <label class="control-label">
                                    Score : 
                                    </label>
                                    <div class="controls">
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="0">
                                      0
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="1">
                                      1
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="2">
                                      2
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="3">
                                      3
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="4">
                                      4
                                    </label>
                                    <label class="radio inline">
                                      <input type="radio" name="scorefromseller" value="5" checked>
                                      5
                                    </label> 
                                    </div>
                                    </div>
                                    <input type="hidden" name="idPost" value="' . $post['idPost'] . '"/>                               
                                    
                                    
                                    <div class="input-prepend input-append" style="display: block;">
                                      
                                            <span class="add-on" disabled><i class="icon-user"></i> Seller</span>&nbsp;
                                        
                                        <input class="input-xlarge" id="appendedPrependedInput" name="feedbackfromseller" type="text" placeholder="Feedbacks to buyer">
                                        <div class="btn-group">
                                            <input class="btn btn-info btn" type="submit" value="Send">
                                        </div>
                                        
                                    </div>
                                    
                                </form>
                            </div>';
}

function generateUrlByPostId($postId) {
    $url = "viewPostDetail?idPost=" . $postId;
    echo $url;
}

function findImg($postID) {
    $img = Post::model()->findByPk($postID)->productImage;

    $filename = Yii::app()->baseUrl . "/clients_upload/" . $img;
    if (empty($img))
        return "../clients_upload/null.JPG";
    else
        return $filename;
}

function showPostPreview($type, $status, $postName, $price, $seller, $detail, $postId, $postDate) {
    $price = number_format($price);
    $name = Member::model()->findByPk($seller);
    echo '<div class="span4" style="background-color: #e6e6e6; border-radius: 5px; position: relative">
        <a class="pricePost btn btn-info pull-right disabled" style="position: absolute; right: 0px; bottom: 0px; margin: 5px">฿ ' . $price . '</a>'
                   . '<div style="position: absolute; top: 1px; left: 3px">';
    if ($status == "Open") {
        if ($type == "ac")
            echo '<span class="label label-important">Auction';
        // else echo '<span class="label label-important">Direct sale';
    } if ($status == "Purchased") {
        echo '<span class="label label-inverse">Purchased';
    }
    echo '</span>';
    echo '</div>
                    <a class="pull-left">
                        <img class="img-polaroid" src="' . findImg($postId) . '" style="width: 100px; height: 100px; margin: 7.5px;">
                    </a>

                    <div class="row" style="top: 0;  position: absolute; font-size: small; margin-bottom: 5px; width: 100%">
                        <div class="span4" style="width: 100%; text-align: justify">
                            <div style="position: absolute; margin-left: 130px; width: 230px; text-align: justify">
                            <a style="color: black" href="';
    echo generateUrlByPostId($postId);
    echo '"><h4 id="postName">' . $postName . '</h4></a>
                            <span>' . substr($detail, 0, 200) . '</span></div>
                        </div>
                    </div>

                    <div class="row" style="bottom: 0;  position: absolute; font-size: small; margin-bottom: 5px;">
                        <div class="span4" style="padding-left: 125px; width: 100%; position: relative">
                            
                                <span style="position: relative; right: 0px; bottom: 0px; font-size: small;">'.
                                '<div class="postBox" style="display: block;white-space: nowrap; overflow:hidden !important; text-overflow: ellipsis; left: 110px; top: 100px">' .
                                '<i class="icon-user"></i>&nbsp;<a href="../member/viewMemberDetail?idMember=' . $name->idMember . '">' . $name->username . '</a>'
                                . '&nbsp;<span style="font-size: xx-small;">(' . $postDate . ')</span><br/>'
                                . '</div>
                            </span>
                            
                        </div>
                    </div>

                </div>';
}

function showPost($type, $status, $postName, $price, $seller, $date, $postId) {
    $price = number_format($price);
    $name = Member::model()->findByPk($seller);

    if ($type == "ac" && $status == "Open") {
        echo '<td style="text-align:center"><a href="#" rel="tooltip" data-original-title="ราคาประมูลปัจจุบัน ' . $price
        . ' บาท"><span class="label label-important">Auction</span></td>';
    } else
    if ($type == "ds" && $status == "Open") {
        echo ' <td style="text-align:center">
                          <!--<a href="#" rel="tooltip" data-original-title="ราคา ' . $price . ' บาท"><span class="label label-important">Direct sale</span></a>--> 
                    </td>';
    } else
    if ($type == "ds" && $status == "Purchased") {
        echo '<td style="text-align:center">
                         <a style="color:gray" rel="tooltip" data-original-title="ราคาขาย ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
                    </td>';
    } else
    if ($type == "ac" && $status == "Purchased") {
        echo '<td style="text-align:center">
                        <a style="color:gray" rel="tooltip" data-original-title="ปิดประมูลที่ ' . $price . ' บาท"><span class="label label-inverse">Purchased</span></a>
                    </td>';
    }
    echo '<td><a href=';
    generateUrlByPostId($postId);
    echo '>' . $postName . '</td>
                    <td style="text-align:center">' . $name->username . '</td>
                    <td style="text-align:center">' . $date . '</td><tr>';
}
?>


<div class="main_div" style="width: 100%; margin-left: 0px">
    <?php
    if (isset($_GET['reportPost']) && $_GET['reportPost'] == 1) {
        ?>
        <div style="display: block;">
            <div class="alert alert-success" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Thank you for report problem. </strong><br>เจ้าหน้าที่จะทำการตรวจสอบและแก้ปัญหาให้โดยทันที<br>
            </div>
        </div>
    <?php } else if (isset($_GET['reportPost']) && $_GET['reportPost'] == 0) { ?>
        <div style="display: block;">
            <div class="alert alert-error" style="margin: 15px 50px;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Report Fail!</strong><br>โปรดกรอกรายละเอียดของปัญหาที่จะร้องเรียนอีกครั้ง<br>
            </div>
        </div>

    <?php } ?>


    <?php
    if (!isset($allPost)) { //if allpostopen is null
        echo '<div class="span11" style="border:5px solid rgb(200,200,200);border-radius:20px;padding:15px;position:relative;margin-top:20px">';
        echo '<br><div class="span4">';
        
            echo '<img style="width: 100%" src="' . findImg($post['idPost']) . '">';


        /*
          echo '<h3><a href="index" ><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3_HQdxK9E

          6yqM-UJxUZGYlZdPEXaSpnGXg1UsKbFJGvGdJJQN" class="img-circle" width="70px" height="70px">Back to post list.</a></h3>
         */
        echo '</div>'; //image post and back buttom

        echo '<div class="span6">';
        echo '<div id="status">Status : ' . $post['status'] . '</div>'; //status box
        echo '<p align="center" style="border-radius: 5px; font-size:28px;background-color:rgb(255,100,100);color:rgb(255,255,255);padding:10px"> ' . $post['productName'];
        echo '<hr /><b>Product detail</b> : <div style="margin: 10px; margin-bottom: 2em;">' . $post['productDetail'] . '</div>';
        ?>
        <div class="main_div">


            <table class="table table-bordered table-striped">
                <tbody>
                    <?php
                    if (isset($sellerUsername))
                        echo '<tr><td style="width: 25%">Seller</td><td><i class="icon-user"></i>&nbsp;<a href="../member/viewMemberDetail?idMember=' . $sellerID . '">' . $sellerUsername . '</a></td></tr>';
                    if (isset($buyerUsername) && $post['status'] == "Purchased")
                        echo '<tr><td>Buyer</td><td><i class="icon-user"></i>&nbsp;<a href="../member/viewMemberDetail?idMember=' . $buyerID . '">' . $buyerUsername . '</a></td></tr>';
                    echo '<tr><td>Type</td><td>' . (($post['type'] == 'ac')? "Auction" : "Direct sale") . '</td></tr>';
                    echo '<tr><td>' . (($post['type'] == 'ac')? "Current price" : "Price") . '</td><td>฿ ' . number_format($post['price']) . '</td></tr>';
                    //echo '<div class="span2" style="margin:0px">';


                    if ($post['type'] == 'ac')
                        echo ' <tr><td>Auction end time</td><td>' . $auction['closeDateTime'] . '</td></tr> ';
                    ?>                          
                </tbody>
            </table>

            <?php
            if (!isset($post['buyerMemberIdMember']) && $post['type'] == "ds" && Yii::app()->user->getState('cID') != $sellerID && Yii::app()->user->getState('cID') != "")
//if this post not sell. show buy bottom  BUY BOTTOM
                echo '<a href="Buy?idPost=' . $post->idPost . '" class="btn btn-primary btn-large"><i class="icon-shopping-cart icon-white"></i>&nbsp;Buy</a>&nbsp;&nbsp;&nbsp;&nbsp;';

            if (isset($post['Buyer_Member_idMember']) && $post['type'] == "ac" && Yii::app()->user->getState('cID') == $post['Buyer_Member_idMember']) {
                $invoice = Invoice::model()->findByPk($_GET['idPost']);
//                if (!isset($invoice))
////if this post not sell. show buy bottom  BUY BOTTOM
//                    echo '<a href="Buy?idPost=' . $post->idPost . '" class="btn btn-primary btn-large">Buy</a>&nbsp;&nbsp;&nbsp;&nbsp;';
            }


            if (Yii::app()->user->getState('cID') == $sellerID) { //EDIT POST
                echo '<a class="btn btn-warning' . (($post['status'] == "Purchased")? ' disabled' : '" href="editPost?idPost=' . $post->idPost ) . '"><i class="icon-pencil icon-white"></i>&nbsp;Edit this post</a> ';
            }
            if (isset($post['buyerMemberIdMember']) && $post['type'] == "ds" && $post['status'] == "Purchased" && Yii::app()->user->getState("cID")  != null)
                echo '<a class="btn btn-info" href="../invoice/viewInvoice?postID=' . $post->idPost . '"><i class="icon-file icon-white"></i>&nbsp;View invoice</a> ';
            ?>
            <?php if (isset($sellerID) && $sellerID != Yii::app()->user->getState("cID") &&  Yii::app()->user->getState("cID")  != null) { ?>

                <a href="#report" role="button" class="btn btn-danger pull-right" data-toggle="modal"><i class="icon-comment icon-white"></i>&nbsp;Report this post</a>

                <div id="report" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordTitle" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3>Report this post</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="reportpost">
                        <div class="modal-body">
                            <div class="control-group">
                                <label class="control-label required" for="Member_username">ร้องเรียน</label>
                                <div class="controls">
                                    <input name="username" disabled="disabled" value="<?php echo $post['productName'] . " (" . $sellerUsername . ")"; ?>" type="text">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">รายละเอียด</label>
                                <div class="controls">
                                    <textarea rows="5" type="text" name="message"></textarea>
                                </div>
                            </div>
                            <input type="hidden" name="referred_idMember" value="<?php echo $sellerID; ?>">
                            <input type="hidden" name="Post_idPost" value="<?php echo $post->idPost; ?>">
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                            <button class="btn btn-primary" type="submit">Report</button>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div></div>
    <?php
//            echo '</div><div class="span3" style="margin:0px">';
//
//
//            echo '</div></div>';


    if ($post->status == 'Purchased' && isset($_GET['idPost'])) {
        if (Yii::app()->user->getState('cID') == $post->Buyer_Member_idMember || Yii::app()->user->getState('cID') == $post->Seller_Member_idMember) {
            showBuyStage($post);  //PROCESS OF BUY -------------------------------------
        }
    }

    $feed = Feedback::model()->findByPk($post['idPost']);
    if (isset($feed)) {
        echo '<div class="span10" style="border:1px solid rgb(220,220,220);padding:20px;margin:20px; border-radius: 5px">';
        if ($feed->feedbackFromBuyer != NULL) {
            echo '<h4><p>Feedback from buyer to seller :</p></h4>' . $feed->feedbackFromBuyer;
        }

        if ($feed->feedbackFromSeller != NULL) {
            echo '<h4><p>Feedback from seller to buyer :</p></h4>' . $feed->feedbackFromSeller;
        }

        echo '</div>';
    }


    if ($post['type'] == 'ac') { //auction show this table
        if ($post['status'] == "Open" && Yii::app()->user->getState('cID') != $sellerID && Yii::app()->user->getState('cID') != "") {
            if (isset($error))
                echo '<p style="color:red" class="span5">**** Bidding price must be more than mininum rate.****</p>';
            echo '<div class="span10" style="margin-top:10px;border:2px solid rgb(255,200,200);border-radius:20px;padding:15px"><table width="100%" align="center">
    <tr>
        <td>
                <p align="center" style="color:rgb(255,255,255);background-color:rgb(255,100,100);padding:5px">Current price : ฿ ' . number_format($auction['currentPrice']) . '</p>
                <h3><p align="center">Minimum Bid Rate : ฿ ' . number_format($auction['minimumBidRate']) . '</p></h3>
                    <form class="form-horizontal" method="POST" action="bid">
                    <div class="control-group">
                        <label class="control-label" for="bid">Add price</label>
                    <div class="controls">
                        <div class="input-prepend">
                        <span class="add-on"><i class="icon-plus"></i> ฿</span>
                        <input type="text" id="inputIcon" name="bid" size="20">
                        </div>
                    </div>
                    </div>
                        <input type="hidden" value="' . Yii::app()->user->getState('cID') . '" name="idMember">
                        <input type="hidden" value="' . $post['idPost'] . '" name="idPost">           
                        <div class="controls"><input class="btn" type="submit" value="Add" name="Bid"><!--<input type="reset" value="Reset" name="B4">--></div>
                    </form>
                </td>
                <td><p align="center" style="color:rgb(255,255,255);background-color:rgb(255,100,100);padding:5px">Autobid</p>
            
                        <h3><p align="center">Your autobid now: ';
            if (isset($autobid['autobidRate'])) {
                echo '' . number_format($autobid['autobidRate']) . '';
            } else {
                echo ' - ';
            }

            echo '</h3></p><form class="form-horizontal" method="POST" action="autobid">
                        <div class="control-group">
                        <label class="control-label" for="bid">Highest price</label>
                    <div class="controls">
                        <div class="input-prepend">
                        <span class="add-on">฿</span>
                        <input type="text" name="autobid" size="20">
                        </div>
                        </div>
                        </div>
                        <input type="hidden" value="' . Yii::app()->user->getState('cID') . '" name="idMember">
                        <input type="hidden" value="' . $post['idPost'] . '" name="idPost">           
            <div class="controls"><input class="btn" type="submit" value="Activate" name="Bid"><!--<input type="reset" value="Reset" name="B4">--></div>
        </form>
                </td>
    </tr>
</table></div>';
        }

        echo '<div class="span8" style="margin: 30px;padding-left:100px; border-radius: 5px"><table class="table table-hover table-condensed" width="40%" align="center">
    <thead>
    <tr >
        <th style="width: 25%; text-align: center">Date & time</th>
        <th style="width: 30%; text-align: center">Username</th>
        <th style="width: 25%; text-align: center">Price</th>
        <th style="width: 15%;text-align: center">Auto-bid</th>
    </tr></thead>';
        foreach ($allBid as $bid) { //show bid challenge table
            echo '<tr><td style="text-align: center">' . date("Y-m-d H:i:s", strtotime($bid['timestamp'])) . '</td><td>' . $bid['buyerUsername'] . '</td><td>฿ ' . number_format($bid['price']) . '</td>';
            if ($bid['autobid']) {
                echo '<td style="text-align: center"><i class="icon-ok"></i></td>';
            } else {
                echo '<td></td>';
            }
            echo '</tr>';
        }

        echo '</table>';
    }
    echo '</div></div>';
} else {
    ?>

    <?php
    $this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
        'heading' => CHtml::encode(Yii::app()->name)));
    $this->endWidget();
    ?>  

    <div class="pull-left">
        <!-- AddPost Button -->
    <?php
    if (!Yii::app()->user->isGuest) {
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => '<i class="icon-plus-sign icon-white"></i> Add post',
            'type' => 'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'url' => array('/post/addPost'),
            'encodeLabel' => false,
            'size' => 'large',
        ));
    }
    ?>
    </div>
    <form class="form-search" method="GET">
        <div align="center">
            <div class="input-append">

                <input name="search" type="text" placeholder="Type keyword here" class="input-xlarge search-query">
                <button type="submit" class="btn"><i class="icon-search"></i></button>
            </div>
        </div>
    </form>

    <!-- PostList -->

    <br />
    <?php
    for ($i = 0; $i < count($allPost); $i++) {
        if ($i % 3 == 0)
            echo "<div class=\"row\" style=\"margin-bottom: 10px\">";
        showPostPreview($allPost[$i]->type, $allPost[$i]->status, $allPost[$i]->productName, $allPost[$i]->price, $allPost[$i]->Seller_Member_idMember, $allPost[$i]->productDetail, $allPost[$i]->idPost, date_format(date_create($allPost[$i]->postDateTime), 'd/m/Y'));
        if ($i % 3 == 2 || $i == count($allPost))
            echo "</div>";
    }
    ?>

    <?php array('label' => 'Contact', 'url' => array('/site/contact')); ?>

    <div align="right" style="clear: both; margin-top: 10px">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
    ))
    ?>
        <br />
    </div>
    <?php } ?>
</div>

<style type="text/css">

    #status{
        padding: 20px;
        background-color: rgb(245,240,250);
        width: 230px;
        position: absolute;
        border-radius: 200px;
        border: 5px solid rgb(255,155,55);
        top: -40px;
        left: -40px;
        //font-family: cursive;
        font-size: 24px;
        text-align: center;
        color: rgb(255,10,100)
    }


</style>

<?php /*
  $gridDataProvider = new CArrayDataProvider(array(
  array('id'=>1, 'type'=>'Auction', 'postName'=>'Mark', 'by'=>'Otto', 'date'=>'CSS'),
  array('id'=>2, 'type'=>'DirectSale', 'postName'=>'Jacob', 'by'=>'Thornton', 'date'=>'JavaScript'),
  array('id'=>3, 'type'=>'Purchased', 'postName'=>'Stu', 'by'=>'Dent', 'date'=>'HTML'),
  ));

  $this->widget('bootstrap.widgets.TbGridView', array(
  'dataProvider'=>$gridDataProvider,
  'template'=>"{items}",
  'columns'=>array(
  array('name'=>'type', 'header'=>'Type'),
  array('name'=>'postName', 'header'=>'Post Name'),
  array('name'=>'by', 'header'=>'Seller'),
  array('name'=>'date', 'header'=>'Date'),

  ),
  ));


  <p>You may change the content of this page by modifying the following two files:</p>

  <ul>
  <li>View file: <code><?php echo __FILE__; ?></code></li>
  <li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
  </ul>

  <p>For more details on how to further develop this application, please read
  the <a href="http://www.yiiframework.com/doc/">documentation</a>.
  Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
  should you have any questions.</p>

 */
?>

<script type="text/javascript">
    $(document).ready(function(){
        $(".postBox").width($(".postBox").width() - $(".pricePost").width());
    });
</script>