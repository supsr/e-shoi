<?php ?>
<div id="addPostHead"><h2>Edit Post</h2></div><br><br><br>

<div id="addPostBody" class="span8">

    <div style="margin-left:120px;">
    <form class="form-horizontal" method="POST" action="edit" enctype="multipart/form-data">
        <input type="hidden" value="<?php echo Yii::app()->user->getState('cID'); ?>" name="Member_idMember" </p>   
        <input type="hidden" value="<?php echo $post['idPost'] ?>" name="idPost" </p>   
        <div class="control-group">
            <label class="control-label" for="inputProductName">Product name</label>
            <div class="controls">
                <input class="input-xlarge" type="text" id="inputProductName" name="productName" value= "<?php echo $post['productName']; ?>" placeholder="Product name" size="20" required="">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputProductDetail">Product detail</label>
            <div class="controls">
                <textarea id="inputProductDetail" name="productDetail" placeholder="Product detail" size="30" rows="4" style="width: 20em"><?php echo $post['productDetail']; ?></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputProductImage">Image URL</label>
            <div class="controls" >
                <input type="file" name="productImage" id="inputProductImage" accept="image/*">
            </div>
            <?php if(isset($post['productImage']) && !empty($post['productImage'])) {?>

            	<div style="margin-left:185px;">default: <br /><img width="200" src="<?php echo Yii::app()->baseURL . "/clients_upload/" . $post['productImage']; ?>"></div>
            <?php } ?>
        </div>
       
        <div class="control-group">
            <label class="control-label" for="type">Type</label>
            <div class="controls">
                <select size="1" name="type" id='type' disabled='disabled'>
                	<?php if(isset($post['type']) && $post['type']=='ds') {?>
                    <option value="ds" selected>Direct Sale</option>
                    <?php } else { ?>
                    <option value="ac" selected>Auction</option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="type">Status</label>
            <div class="controls">
                <select size="1" name="status" id='type' <?php if(isset($post['status']) && $post['status']=='Purchased') echo 'disabled="disabled"'?>>
                	<?php if(isset($post['status']) && $post['status']=='Open') {?>
                    <option value="Open" selected>Open</option>
                    <option value="Canceled" >Cancel</option>
                    <?php } else if(isset($post['status']) && $post['status']=='Canceled') { ?>
                    <option value="Canceled" selected>Cancel</option>
                    <option value="Open" >Open</option>
                    <?php } else if(isset($post['status']) && $post['status']=='Purchased') { ?>
                    <option value="Purchased" selected>Purchased</option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <?php if(isset($post['type']) && $post['type']=='ds') {?>
        <div class="control-group">
            <label class="control-label" id="labelInitialPrice" for="inputInitialPrice">Price</label>
            <div class="controls">
                <div class="input-prepend input-append">
                    <span class="add-on">฿</span>
                    <input id="inputInitialPrice" class="input-medium" type="text" name="price" size="20" style="text-align: right" value="<?php echo $post['price'];?>" placeholder="Initial price" required <?php if(isset($post['status']) && $post['status']=='Purchased') echo 'disabled="disabled"'?> >
                    <span class="add-on">.00</span>
                </div>
            </div>
        </div>

        <?php } else if(isset($post['type']) && $post['type']=='ac') {?>
        <div class="control-group">
            <label class="control-label" id="labelInitialPrice" for="inputInitialPrice">Current price</label>
            <div class="controls">
                <div class="input-prepend input-append">
                    <span class="add-on">฿</span>
                    <input disabled="disabled" class="input-medium" type="text" style="text-align: right" value="<?php echo $auction['currentPrice'];?>">
                    <span class="add-on">.00</span>
                </div>
            </div>
        </div>

        <div id="ac_part">
            <div class="control-group">
                <label class="control-label" for="inputMinimumBidRate">Minimum bidding price</label>
                <div class="controls">
                    <div class="input-prepend input-append">
                        <span class="add-on">฿</span>
                        <input class="input-medium" type="text" name="minimumBidRate" size="20" style="text-align: right" value="<?php echo $auction['minimumBidRate'];?>" placeholder="Minimum bidding price" <?php if(isset($post['status']) && $post['status']=='Purchased') echo 'disabled="disabled"'?>>
                        <span class="add-on">.00</span>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputCloseDateTime">Auction ending time</label>
                <div class="controls" style="margin-top:5px;">
                	<?php echo $auction['closeDateTime']; ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div style="margin-left:450px;">
                <input class="btn btn-large btn-primary" type="submit" value="Edit post" name="B1">
				<a class="btn" href="viewPostDetail?idPost=<?php echo $post['idPost']?>">cancel</a>        
		</div>
    </form>
    <div>
<!--    <a href="post" ><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3_
                                                 HQdxK9E6yqM-UJxUZGYlZdPEXaSpnGXg1UsKbFJGvGdJJQN" class="img-circle" width="50px" height="50px"></a>-->
</div>
</div>

<script language="JavaScript">
    $('#type').on("change", function() {
        var selected_value = $("#type option:selected").val();
        if (selected_value == "ac") {
            fncShow('ac_part');
            document.getElementById('inputInitialPrice').placeholder = 'Initial Price';
            document.getElementById('labelInitialPrice').innerHTML = "Initial Price";
            document.getElementById('inputMinimumBidRate').required = true;
            document.getElementById('inputCloseDateTime').required = true;
        }
        if (selected_value == "ds") {
            fncHide('ac_part');
            document.getElementById('inputInitialPrice').placeholder = 'price';
            document.getElementById('labelInitialPrice').innerHTML = "price";
            document.getElementById('inputMinimumBidRate').required = false;
            document.getElementById('inputCloseDateTime').required = false;
        }
    });

    function fncShow(ctrl) {
        document.getElementById(ctrl).style.display = '';
    }

    function fncHide(ctrl) {
        document.getElementById(ctrl).style.display = 'none';
    }
</script>
