<?php
/* @var $this PurchasedController */

?>

<p>
    <tt><?php //echo __FILE__; ?></tt>
</p>

<?php 

    function generateUrlByPostId($postId){
        $url = "../post/viewPostDetail?postId=".$postId;
        echo $url;
    }

    function generateSearchPostUrlByPostName($postName){
        $get_postName = "?postName=".$postName;
        echo Yii::app()->request->url;
        echo $get_postName;
    }

    function showPost($postId,$postName,$status,$seller,$purchasedDate){
        if($status != "complete"){
            echo '<tr><td style="text-align:center">
                       <div class="btn-group">
                        <a class="btn btn disabled">'.$status.'</a>
                   </div>
                </td>';
        } else

        if($status == "complete"){
            echo '<td style="text-align:center">
                    <div class="btn-group">
                     <a class="btn btn-inverse disabled" style="color:gray">Complete</a>
                    </div>
                </td>';
        }

        echo   '<td><a href=';generateUrlByPostId($postId);  
        echo   '>'.$postName.'</td>
                <td style="text-align:center">'.$seller.'</td>
                <td style="text-align:center">'.$purchasedDate.'</td><tr>';

    }
?>

<?php
/* @var $this MemberController */

 function generateUrlByPostId_myPost($postId){
        $url = "http://localhost/e-shoi/post/viewPostDetail?postId=".$postId;
        echo $url;
    }

    function generateSearchPostUrlByPostName_myPost($postName){
        $get_postName = "?postName=".$postName;
        echo Yii::app()->request->url;
        echo $get_postName;
    }

    function showPost_myPost($type,$status,$postName,$price,$seller,$buyer,$date,$postId){
        if($type == "Auction" && $status != "purchased"){

            echo '<tr><td style="text-align:center">
                       <div class="btn-group">
                        <a class="btn btn disabled">Auction</a>
                         <a class="btn btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a><i class="icon-play"></i>12/07/14 :12.00</a></li>
                            <li><a><i class="icon-pause"></i>14/07/14 :12.00</a></li>
                            <li>เหลือเวลา 2วัน0ชั่วโมง0นาที</li>
                            <li class="divider"></li>
                            <li><a><i class="i"></i> ราคาปัจจุบัน '.$price.' บาท</a></li>
                         </ul>
                   </div>
                </td>';
       } else 
        if($type == "DirectSale" && $status != "purchased"){
            echo ' <td style="text-align:center">
                      <a href="#" class="btn btn disabled" rel="tooltip" data-original-title="ราคา '.$price.' บาท">DirectSale</a> 
                </td>';
        } else
        if($type == "DirectSale" && $status == "purchased"){
            echo '<td style="text-align:center">
                    <div class="btn-group">
                     <a class="btn btn-inverse disabled" style="color:gray" rel="tooltip" data-original-title="ราคาขาย '.$price.' บาท">Purchased</a>
                    </div>
                </td>';
        } else
        if($type == "Auction" && $status == "purchased"){
            echo '<td style="text-align:center">
                    <div class="btn-group">
                        <a class="btn btn-inverse disabled" style="color:gray" rel="tooltip" data-original-title="ปิดประมูลที่ '.$price.' บาท">Purchased</a>
                    </div>
                </td>';
        }
        echo   '<td><a href=';generateUrlByPostId_myPost($postId);  
        echo   '>'.$postName.'</td>
                <td style="text-align:center">'.$seller.'</td>
                <td style="text-align:center">'.$buyer.'</td>
                <td style="text-align:center">'.$date.'</td><tr>';

    }

?>


<ul class="nav nav-tabs">
    <li><a href="#myPost" data-toggle="tab"><h5>MyPost<h5></a></li>
    <li><a href="#myPurchased" data-toggle="tab"><h5>MyPurchased</h5></a></li>
</ul>


<div class="tab-content">
    <div class="tab-pane active" id="myPost">
        <div class="main_div">
        <h1 align="center">My Post</h1>

        <!-- PostList -->
        <table class="table">
            <thead>
                <tr>
                    <th class="span1" bgcolor="#e74c3c" style="color:white">
                        <div align="center" >Type</div>
                    </th>
                    <th class="span5" bgcolor="#e74c3c" style="color:white">Post Name</th>
                    <th class="span2" bgcolor="#e74c3c" style="color:white">
                        <div id="type"align="center" >Seller</div>
                    </th>
                    <th class="span2" bgcolor="#e74c3c" style="color:white">
                        <div id="type"align="center" >Buyer</div>
                    </th>
                    <th class="span2" bgcolor="#e74c3c" style="color:white">
                        <div id="type"align="center">Date</div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php showPost_myPost("DirectSale","on","ทีวี Sony 21นิ้ว จอแบน สภาพสวย ใช้งานได้ปรกติ ของดีมาเอาไปใช้","1990","Pakko","-","20/11/57 20:02:22น.","1");?>
                <?php showPost_myPost("Auction","on","ขายเครื่องเสียงมือสองจากญี่ปุ่นแท้ๆ","350","Pakko","-","19/11/57 20:02:22น.","2");?>
                <?php showPost_myPost("DirectSale","purchased","ตู้เย็นเล็กแบบพกพา ใช้ได้ทั้งไฟรถ และไฟบ้านครับ","400","Pakko","BuyerName","18/11/57 20:02:22น.","3");?>
            </tbody>
        </table>
        <div class="pagination pagination-right">
        <ul>
            <li class="disabled"><a href="#">&laquo;</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">&raquo;</a></li>               
        </ul>
        </div>
    </div>

    </div>
    <div class="tab-pane" id="myPurchased">
        <h1 align="center">My Purchased Post</h1>
        <div class="mainPurchasedList_div">
            <p>
                <!-- PostList -->
                <table class="table">
                    <thead>
                        <tr>
                            <th class="span1" bgcolor="#27ae60" style="color:white">
                                <div align="center" >Status</div>
                            </th>
                            <th class="span4" bgcolor="#27ae60" style="color:white">Post Name</th>
                            <th class="span2" bgcolor="#27ae60" style="color:white">
                                <div id="type"align="center" >Seller</div>
                            </th>
                            <th class="span2" bgcolor="#27ae60" style="color:white">
                                <div id="type"align="center">Purchased Date</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php showPost("01234","ทีวี Sony 21นิ้ว จอแบน สภาพสวย ใช้งานได้ปรกติ ของดีมาเอาไปใช้","sending","PongTheSeller","20/11/57 20:02:22น.");?>
                        <?php showPost("01234","ขายเครื่องเสียงมือสองจากญี่ปุ่นแท้ๆ","complete","Ebay","20/11/57 19:02:22น.");?>
                        <?php showPost("01234","ตู้เย็นเล็กแบบพกพา ใช้ได้ทั้งไฟรถ และไฟบ้านครับ","complete","OverClock","/11/57 20:02:22น.");?>
                    </tbody>
                </table>
                <div class="pagination pagination-right">
                    <ul>
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">&raquo;</a></li>               
                    </ul>
                </div>
            </p> 
        </div>
    </div>
</div>

    

