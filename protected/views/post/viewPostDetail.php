<?php
/* @var $this SiteController */
   $productId =  $_GET['postId'];
   $productName = 'Marshall Major';
   $productDetail = 'Product Specifications

    Over-ear headphones with classic Marshall styling and technology
    Compatible with most devices using 3.5mm standard audio plug (in-line volume/call control only works with iOS products)
    Perfect for classic rock audiophiles
    Transducer: 40mm Moving Coil Dynamic Speaker
    Impedance: 47 ohms
    Sensitivity: 100mV@1kHz = 98dB SPL
    Frequency Response: 20Hz~20kHz
    WBCV: 124mW
    Materials: vinyl (the same vinyl they use to cover the amps)
    Inside of adjustable headband is inscribed "London England 1962"
    Includes: collapsible headphones, microphone, and remote, in-line volume/call control for iOS products, canvas carrying case
    Dimensions: 7.4" x 7.3" x 3.9" inches (collapsible)';

   $productImage = 'http://cdn.slashgear.com/wp-content/uploads/2011/11/2743_5634e4005e-original.png';
   $status = 'purchased';
?>

<br>

<div class='row' style='border:5px solid black;border-radius:10px;position:relative'>
  
	<div class='span7' style='border:2px solid black;margin: 10px;border-radius:5px'>
		<? echo '<img src = "clients_upload\'.$productImage.'">' ?>
	</div>

	<div class='span5' style='margin: 10px'>
		<? echo '<p><h1>'.$productName.'</h1></p>' ?>
		 <hr width="450"> 
		<? echo '<p>'.$productDetail.'</p>' ?>
	</div>

	<div class='btn btn-large btn-info' style='margin:10px'>
		Buy now
	</div>

	<div id='status'>
		<? echo $status; ?>
	</div>

           <br><br>
</div>

<br><br>

<div class='row'>
	<p>You may change the content of this page by modifying the following two files:</p>

	<ul>
	    <li>View file: <code><?php echo __FILE__; ?></code></li>
	    <li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
	</ul>
</div>

<style type="text/css">

#status{
	padding: 20px;
	background-color: rgb(250,220,220);
	width: 200px;
	position: absolute;
	border-radius: 50px;
	top: -30px;
	right: -50px;
	font-family: cursive;
	font-size: 24px;
	text-align: center;
}

</style>






