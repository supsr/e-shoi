
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <?php

    function space($n = 2) {
        echo str_repeat("&nbsp;", $n);
    }

    function strTel($strNumber) {

        if (is_numeric($strNumber) && strlen($strNumber) > 8 && strlen($strNumber) < 11) {
            $i = strlen($strNumber);
            $newStrNumber = "";
            while ($i >= 4) {
                $newStrNumber = "-" . substr($strNumber, $i - 4, 4) . $newStrNumber;
                $i -= 4;
            }
            $newStrNumber = substr($strNumber, 0, $i) . $newStrNumber;
            return $newStrNumber;
        } else {
//            echo strpos($strNumber, "-");
            if (strlen($strNumber) > 10 && strlen($strNumber) < 13 && preg_match_all('/^[0-9]{2,3}[-]{0,1}[0-9]{3,4}[-]{0,1}[0-9]{4,4}$/i', $strNumber)) {
                $newStrNumber = str_replace("-", "", $strNumber);
            }
            return strTel($newStrNumber);
        }
    }
    ?>
    <head><style type="text/css">
            .thaiFontXL {
                font-family: 'thsarabun';
                font-size: 24px;
            }

            .thaiFontL {
                font-family: 'thsarabun';
                font-size: 20px;
            }

            .thaiFontM {
                font-family: 'thsarabun';
                font-size: 18px;
            }

            .thaiFontS {
                font-family: 'thsarabun';
                font-size: 16px;
            }

            .left {
                text-align: left;
            }
            .center{
                text-align: center
            }
            .right {
                text-align: right;
            }

            .bold{
                font-weight: bold;
            }
            .borderBLK{
                border: 0.3mm solid black;
            }
            .border2{
                border: 0.3mm dashed black;
                border-bottom: 0px;
                border-left: 0px;
                border-right: 0px;
            }
        </style>
    </head>
    <?php
    $colspanAllS1 = 2;
    $colspanUsername = 1;
    $colspanAll = 12;
    $colspanT1 = 8;
    $colspanImg = 4;
    $colspanOptionT2 = 3;
    $rowspanImg = 9;
    $colspanFBT3 = 5;
    $colspanType = 4;
    ?>
    <body>
        <div class="thaiFontXL center bold">ใบกำกับรายการสินค้า</div>
        <div class="center bold" style="font-family: dejavusanscondensed">INVOICE</div>
        <br />
        <table class="borderBLK" style="width: 100%; border-collapse: collapse; margin-top: 20px" cellspacing="0">
            <thead>
                <tr>
                    <th class="thaiFontL bold left" colspan="<?php echo $colspanAllS1; ?>" style="background-color: #cccccc"><?php space(); ?>1. ข้อมูลผู้ซื้อ</th>
                </tr>
            </thead>
            <tbody >
                <tr>
                    <td class="thaiFontS left" colspan="<?php echo $colspanAllS1 - $colspanUsername; ?>">
                        <?php space(4); ?><b>ชื่อ - นามสกุล<?php space(); ?>:<?php space(); ?></b><?php echo $buyer->firstname . " " . $buyer->lastname; ?>
                    </td> 
                    <td class="thaiFontS left" colspan="<?php echo $colspanUsername; ?>">
                        <b>ชื่อผู้ใช้งาน<?php space(); ?>:<?php space(); ?></b><?php echo $buyer->username; ?>
                    </td> 
                </tr>
                <tr>
                    <td class="thaiFontS left"  colspan="<?php echo $colspanAllS1 - $colspanUsername; ?>">
                        <?php space(4); ?><b>หมายเลขโทรศัพท์<?php space(); ?>:<?php space(); ?></b><?php echo strTel($buyer->telephonenumber); ?>
                    </td>
                    <td class="thaiFontS left"  colspan="<?php echo $colspanUsername; ?>">
                        <b>E-mail ติดต่อ<?php space(); ?>:<?php space(); ?></b><?php echo $buyer->email; ?>
                    </td>
                </tr>
                <tr>
                    <td class="thaiFontS left" colspan="<?php echo $colspanAllS1; ?>"><?php space(4); ?><b>ที่อยู่ที่สามารถติดต่อได้<?php space(); ?>:<?php space(); ?></b><?php echo $buyer->address; ?></td> 
                </tr>
            </tbody>
        </table>
        <div style="height: 5px"></div>
        <table class="borderBLK" style="width: 100%; border-collapse: collapse; margin-top: 5px" cellspacing="0">
            <thead>
                <tr>
                    <th class="thaiFontL bold left" colspan="<?php echo $colspanAllS1; ?>" style="background-color: #cccccc"><?php space(); ?>2. ข้อมูลผู้ขาย</th>
                </tr>
            </thead>
            <tbody >
                <tr>
                    <td class="thaiFontS left" colspan="<?php echo $colspanAllS1 - $colspanUsername; ?>">
                        <?php space(4); ?><b>ชื่อ - นามสกุล<?php space(); ?>:<?php space(); ?></b><?php echo $seller->firstname . " " . $seller->lastname; ?>
                    </td> 
                    <td class="thaiFontS left" colspan="<?php echo $colspanUsername; ?>">
                        <b>ชื่อผู้ใช้งาน<?php space(); ?>:<?php space(); ?></b><?php echo $seller->username; ?>
                    </td> 
                </tr>
                <tr>
                    <td class="thaiFontS left"  colspan="<?php echo $colspanAllS1 - $colspanUsername; ?>">
                        <?php space(4); ?><b>หมายเลขโทรศัพท์<?php space(); ?>:<?php space(); ?></b><?php echo strTel($seller->telephonenumber); ?>
                    </td>
                    <td class="thaiFontS left"  colspan="<?php echo $colspanUsername; ?>">
                        <b>E-mail ติดต่อ<?php space(); ?>:<?php space(); ?></b><?php echo $seller->email; ?>
                    </td>
                </tr>
                <tr>
                    <td class="thaiFontS left" colspan="<?php echo $colspanAllS1; ?>"><?php space(4); ?><b>ที่อยู่ที่สามารถติดต่อได้<?php space(); ?>:<?php space(); ?></b><?php echo $seller->address; ?></td> 
                </tr>
            </tbody>
        </table>
        <br />
        <table class="borderBLK" style="width: 100%; border-collapse: collapse" cellspacing="0">
            <thead>
                <tr>
                    <th colspan="<?php echo $colspanAll; ?>" class="thaiFontL bold left" style="background-color: #cccccc"><center>รายละเอียดการซื้อขายสินค้า<center></th>
                                </tr>
                                </thead>
                                <tbody >
                                    <tr>
                                        <td class="thaiFontM left" colspan="<?php echo $colspanT1; ?>"><?php space(); ?><b>1. ประกาศขายที่เกี่ยวข้อง</b></td> 
                                        <td colspan="<?php echo $colspanImg; ?>" rowspan="<?php echo $rowspanImg; ?>" style="width: 200px; text-align: center"><img style="margin: 10px; width: 150px; height: 150px" src="
                                            <?php
                                            if($_SERVER['DOCUMENT_ROOT'] == "C:/xampp/htdocs") $strPath = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->baseUrl . "/clients_upload/";
                                            else $strPath = "/clients_upload/";
                                            if (!empty($post->productImage)) {
                                                echo $strPath . $post->productImage;
                                            } else {
                                                echo $strPath . "null.jpg";
                                            }
                                            ?>
                                                                                                                                                                    "><br><em><span class="thaiFontS">(รูปภาพสินค้าบนระบบ)<span><em></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1; ?>"><?php space(5); ?><b>ชื่อสินค้า<?php space(); ?>:<?php space(2); ?></b><?php echo $post->productName; ?></td> 
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1; ?>"><?php space(5); ?><b>รายละเอียดสินค้า<?php space(); ?>:<?php space(2); ?></b></td> 
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="thaiFontS left" style="padding-left: 20px; text-align: justify" colspan="<?php echo $colspanT1; ?>"><?php space(8); ?><?php echo $post->productDetail; ?></td> 
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1; ?>"><?php space(5); ?><b>วันที่ประกาศขาย<?php space(); ?>:<?php space(2); ?></b><?php echo $post->postDateTime; ?></td> 
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1 - ($colspanType); ?>"><?php space(5); ?><b>ชนิดประกาศขาย<?php space(); ?>:
                                                                                <?php space(5); ?><b><input type="radio" <?php echo ($post->type == 'ds') ? "checked=\"checked\"" : ""; ?>/><?php space(2); ?>ซื้อขายโดยตรง</td>
                                                                                    <td class="thaiFontS left" colspan="<?php echo $colspanType; ?>"><?php space(5); ?><b><input type="radio" <?php echo ($post->type == 'ac') ? "checked=\"checked\"" : ""; ?>/><?php space(2); ?>ประมูล</td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1; ?>"><?php space(5); ?><b>วันที่ซื้อ<?php space(); ?>:<?php space(2); ?></b><?php echo $invoice->purchasedDateTime; ?></td> 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1; ?>"><?php space(5); ?><b>ราคาตกลงซื้อ-ขาย<?php space(); ?>:<?php space(2); ?></b><?php echo number_format($post->price); ?> บาท</td> 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanT1; ?>"><?php space(); ?></td> 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="thaiFontM left" colspan="<?php echo $colspanAll; ?>"><?php space(); ?><b>2. สถานะปัจจุบันของการซื้อขาย</b></td> 
                                                                                    </tr>
                                                                                    <tr >
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanOptionT2; ?>"><?php space(5); ?><input type="radio" <?php echo ($invoice->status == 'Wait for Paying') ? "checked=\"checked\"" : ""; ?>/><?php space(2); ?>รอการชำระเงินจากผู้ซื้อ</td> 
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanOptionT2; ?>"><input type="radio" <?php echo ($invoice->status == 'Wait for Product') ? "checked=\"checked\"" : ""; ?>/><?php space(2); ?>อยู่ระหว่างการส่งสินค้า</td> 
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanOptionT2; ?>"><input type="radio" <?php echo ($invoice->status == 'Complete') ? "checked=\"checked\"" : ""; ?>/><?php space(2); ?>การซื้อขายเสร็จสมบูรณ์</td> 
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanOptionT2; ?>"><input type="radio" <?php echo ($invoice->status == 'Fail') ? "checked=\"checked\"" : ""; ?>/><?php space(2); ?>ถูกยกเลิกหรือร้องเรียน</td> 

                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="thaiFontS left" colspan="<?php echo $colspanAll; ?>"><?php space(); ?></td> 
                                                                                    </tr>
                                                                                    <?php if ($invoice->status == 'Complete') { ?>
                                                                                        <tr>
                                                                                            <td class="thaiFontM left" colspan="<?php echo $colspanAll; ?>"><?php space(); ?><b>3. คำตอบรับและข้อเสนอแนะจากผู้ซื้อและผู้ขาย</b></td> 
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanAll - $colspanFBT3; ?>"><?php space(5); ?><b>ผู้ซื้อ<?php space(); ?></b></td> 
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanFBT3; ?>"><b>คะแนนที่ผู้ขายให้กับผู้ซื้อ<?php space(); ?>:<?php space(2); ?></b><?php
                                                                                                echo empty($feedback->scoreFromSeller) ? "(ยังไม่มีคะแนน)" : $feedback->scoreFromSeller. " คะแนน (เต็ม 5 คะแนน)";
                                                                                                ?></td> 

                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanAll; ?>" style="padding: auto 20px; text-align: justify">
                                                                                                <?php space(8); ?><?php
                                                                                                echo empty($feedback->feedbackFromSeller) ? "(ไม่มีคำติชม หรือข้อคิดเห็นใด ๆ)" : $feedback->feedbackFromSeller ;
                                                                                                ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanAll - $colspanFBT3; ?>"><?php space(5); ?><b>ผู้ขาย<?php space(); ?></b></td> 
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanFBT3; ?>"><b>คะแนนที่ผู้ซื้อให้กับผู้ขาย<?php space(); ?>:<?php space(2); ?></b><?php
                                                                                                echo empty($feedback->scoreFromBuyer) ? "(ยังไม่มีคะแนน)" : $feedback->scoreFromBuyer . " คะแนน (เต็ม 5 คะแนน)";
                                                                                                ?></td> 

                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanAll; ?>" style="padding: auto 20px; text-align: justify">
                                                                                                <?php space(8); ?><?php
                                                                                                echo empty($feedback->feedbackFromBuyer) ? "(ไม่มีคำติชม หรือข้อคิดเห็นใด ๆ)" : $feedback->feedbackFromBuyer;
                                                                                                ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="thaiFontS left" colspan="<?php echo $colspanAll; ?>"><?php space(); ?></td> 
                                                                                        </tr>
                                                                                    <?php } ?>
                                                                                    </tbody>
                                                                                    </table>

                                                                                    <br />



                                                                                    </body>
                                                                                    </html>