<?php

class AbuseController extends Controller
{
	public function actionCreate()
	{
            $newAbuse = new Abuse;
            $newAbuse->Post_idPost = $_POST['idPost'];
            $newAbuse->Member_idMember = $_POST['idPoster'];
            $newAbuse->message = $_POST['message'];
            $newAbuse ->save();
            $this->render('index');
	}
        
       	public function actionIndex()
	{
            if (isset($_GET['idMember'])) {
                $data['allposts'] = Abuse::model()->findAllByAttributes(array('Member_idMember'=>$_POST['idMember']));
            } else {
                $data['allposts'] = Abuse::model()->findAllByAttributes(array('Member_idMember'=>"1"));
            }
            $this->render('index',$data);
	}
        
        public function actionFormCreate()
        {
            if (isset($_GET['idPost'])) {
                $post = Post::model()->findByPk($_GET['idPost']);
                $data['sellerUsername'] = $post->sellerMemberIdMember->username;
                $data['idPoster'] = 1;
                $data['post'] = $post;
            } else {
                $this->render('index');
            }
            $this->render('formCreate',$data);
        }
        
              
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}