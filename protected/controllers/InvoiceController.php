<?php

Yii::import('application.vendor.*');
require_once('fpdf/fpdf.php');
require_once('fpdf/html2pdf.php');

class InvoiceController extends Controller {
    public function actionBuy(){
        if(isset($_GET['idPost'])){
            $post = Post::model()->findByPk($_GET['idPost']);
            if($post->status == "Open"){
                $post->Buyer_Member_idMember = Yii::app()->user->getState("cID");
                $post->status = "Purchased";
                $post->save();
                $newInvoice = new Invoice;
                $newInvoice->Post_idPost = $_GET['idPost'];
                $newInvoice->status = 'Wait for Paying';
                $newInvoice->save();
            }
            $this->redirect('/e-shoi/post/viewPostDetail?idPost=' . $_GET['idPost']);
        } else $this->redirect('/e-shoi/post/index');
 
    }

    public function actionCreate() {
        $newInvoice = new Invoice;
        $newInvoice->Post_idPost = $_POST['idPost'];
        $newInvoice->processOfBuyer = $_POST['processOfBuyer'];
        $newInvoice->processOfSeller = $_POST['processOfSeller'];
        $newInvoice->status = $_POST['status'];
        $newInvoice->Buyer_Member_idMember = $_POST['Buyer_Member_idMember'];
        $newInvoice->Seller_Member_idMember = $_POST['Seller_Member_idMember'];
        $data['lnvoice'] = $newInvoice;
        // FALSE means don't have to validate
        $newInvoice->save(FALSE);

        $post = Post::model()->findByPk($_POST['idPost']);
        $post->Buyer_Member_idMember = (int) $_POST['Buyer_Member_idMember'];
        $post->status = 'Purchased';
        $data['Post'] = $post;
        $post->save();

        if (isset($post))
            $this->render('index', $data);
    }

    public function actionIndex() {
        if (isset($_GET['idPost'])) {
            $invoice = Invoice::model()->findByPk($_GET['idPost']);
            $data['buyerUsername'] = $invoice->buyerMemberIdMember->username;
            $data['sellerUsername'] = $invoice->sellerMemberIdMember->username;
            $data['invoice'] = $invoice;
        } else {
            $this->render('index');
        }
        $this->render('index', $data);
    }

    public function actionFormCreate() {
        if (isset($_GET['idPost'])) {
            $post = Post::model()->findByPk($_GET['idPost']);
            $data['sellerUsername'] = $post->sellerMemberIdMember->username;
            $data['buyerUsername'] =Yii::app()->user->getState('cID');
            $data['post'] = $post;
        } else {
            $this->render('index');
        }
        $this->render('formCreate', $data);
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */

    public function actionViewInvoice() {
        if (isset($_GET['postID'])) {
            $data['id'] = $_GET['postID'];
            $data['post'] = Post::model()->findByPk($data['id']);
            $data['seller'] = $data['post']->sellerMemberIdMember;
            $data['buyer'] = $data['post']->buyerMemberIdMember;
            if (!empty($data['buyer']) &&(  (Yii::app()->user->getState("cID") == $data['seller']->idMember) ||  (Yii::app()->user->getState("cID") == $data['buyer']->idMember))) {
                $data['invoice'] = Invoice::model()->findByPk($data['id']);
                $data['feedback'] = Feedback::model()->findByPk($data['id']);
                # You can easily override default constructor's params
                $mPDF1 = Yii::app()->ePdf->mpdf('th');
                $mPDF1->SetAutoFont(AUTOFONT_THAIVIET);
                # Load a stylesheet
                $stylesheet = file_get_contents($_SERVER['DOCUMENT_ROOT'] . Yii::app()->theme->baseUrl . "/css/styles.css");
                //$stylesheet = file_get_contents($_SERVER['DOCUMENT_ROOT'] .  '/e-shoi/protected/vendor/mpdf/examples/mpdfstyletables.css');
                $mPDF1->WriteHTML($stylesheet, 1);

                # renderPartial (only 'view' of current controller)
                $mPDF1->WriteHTML($this->renderPartial('viewInvoice', $data, true));
                $footer = '<table class="border2" style="width: 100%; border-collapse: collapse;" cellspacing="0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody >
                                                                            <tr>
                                                                                <td class="thaiFontS">(ส่วนสำหรับเจ้าหน้าที่ระบบ)</td>
                                                                            </tr>
                                                                            <tr>

                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <div style="width: 100%; margin: 10px; display: table;">
                                                                        <div style="display: table-row; width: 400px">
                                                                            <div style="display: table-cell; width: 300px;">
                                                                                <div class="thaiFontM">INVOICE : </div>
                                                                                <div class="barcodecell">
                                                                                    <barcode code="' . EANCreator::createCode($data['post']->idPost, $data['seller']->idMember, $data['buyer']->idMember) . '" type="EAN13" class="barcode" height="0.5" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div><div style="display: tabel-cell; width: 100%; margin-top: 10px">
                                                                        <div>
                                                                            <table style="width: 100%">
                                                                                <tr>
                                                                                    <td class="thaiFontS" style="text-align: left">© Copyrighted by E-shoi</td>
                                                                                    <td class="thaiFontS" style="text-align: right">Powered by Yii-framework & mPDF</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        </div>';
                $mPDF1->SetHTMLFooter($footer);
                # Outputs ready PDF

                $mPDF1->Output("E-shoi_invoice_" . $data['id'], "I");
            } else {
                $this->redirect(Yii::app()->baseURL . "/post/index");
            }
        } else {
            $this->redirect(Yii::app()->baseURL . "/post/index");
        }
    }

    public function viewError() {
        
    }

}
