<?php

class PostController extends Controller {

    public function actionReportpost() {
        $newAbuse = new Abuse;
        $newAbuse->Member_idMember = Yii::app()->user->getState("cID");
        $newAbuse->message = $_POST['message'];
        $newAbuse->referred_idMember = $_POST['referred_idMember'];
        $newAbuse->Post_idPost = $_POST['Post_idPost'];
        date_default_timezone_set('asia/bangkok');
        $newAbuse->dateReport = date('Y-m-d H:i:s');
        if (!empty($newAbuse->message)) {
            $newAbuse->save();
            $this->redirect('viewPostDetail?idPost=' . $newAbuse->Post_idPost . "&reportPost=1");
        } else
            $this->redirect('viewPostDetail?idPost=' . $newAbuse->Post_idPost . "&reportPost=0");
    }

    public function actionAddPost() {
        $data['seller_Member_idMember'] = Yii::app()->user->getState('cID');

        $this->render('addPost', $data);
    }

    public function actionEditPost() {
        $data['post'] = Post::model()->findByPk($_GET['idPost']);
        $data['auction'] = Auction::model()->findByPk($_GET['idPost']);
        if (Yii::app()->user->getState("cID") == $data['post']->Seller_Member_idMember) {
            $this->render('editPost', $data);
        } else
            $this->redirect('viewPostDetail?idPost=' . $_GET['idPost']);
    }

    public function actionAddPostProcess() {
        // addpost before uploading and get id.
        $newPost = new Post;
        $newPost->type = $_POST['type'];
        $newPost->productName = $_POST['productName'];
        $newPost->productDetail = isset($_POST['productDetail']) ? $_POST['productDetail'] : NULL;
        $newPost->productImage = isset($_POST['productImage']) ? $_POST['productImage'] : NULL;
        $newPost->status = $_POST['status'];
        $newPost->Seller_Member_idMember = $_POST['Member_idMember'];
        $newPost->price = (int) $_POST['price'];
        $newPost->save();

        $data['sellerUsername'] = $newPost->sellerMemberIdMember->username;
        $data['post'] = $newPost;
        if ($newPost->type == "ac") {
            $timetosave = TimeAppx::appx(date('Y-m-d H:i:s', strtotime($_POST['closeDateTime'])));
            Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`auction` (`Post_idPost`, `currentPrice`, `autobidRate`, `minimumBidRate`, `closeDateTime`) 
                    VALUES ('" . $newPost->idPost . "', '" . $_POST['price'] . "', '0', '" . $_POST['minimumBidRate'] . "', '" . $timetosave . "');")->query();
            $newAuction = Auction::model()->findByPk($newPost->idPost);
            $data['allBid'] = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $newAuction->Post_idPost), array('order' => 'timestamp DESC, price DESC, buyerUsername ASC'));
            $data['auction'] = $newAuction;
        }
        // now we're startin' file uploading.
        if (isset($_FILES["productImage"])) {

            $file_type = explode(".", $_FILES["productImage"]["name"]);
            $file_type = $file_type[count($file_type) - 1];
            $file_name = $newPost->idPost . "." . $file_type;
            $destination_path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->baseUrl . "/clients_upload/" . $file_name;
            if (move_uploaded_file($_FILES["productImage"]["tmp_name"], $destination_path)) {
                $currentPost = Post::model()->findByPk($newPost->idPost);
                $currentPost->productImage = $file_name;
                $currentPost->save();
                $this->redirect("viewPostDetail?idPost=" . $newPost->idPost);
            }
        }

        $this->redirect("viewPostDetail?idPost=" . $newPost->idPost);
    }

    public function actionCreate() {
        $newPost = new Post;
        $newPost->type = $_POST['type'];
        $newPost->productName = $_POST['productName'];
        $newPost->productDetail = isset($_POST['productDetail']) ? $_POST['productDetail'] : NULL;
        $newPost->productImage = isset($_POST['productImage']) ? $_POST['productImage'] : NULL;
        $newPost->status = $_POST['status'];
        $newPost->Seller_Member_idMember = $_POST['Member_idMember'];
        $newPost->price = $_POST['price'];
        $newPost->save();
        $data['sellerUsername'] = $newPost->sellerMemberIdMember->username;
        $data['sellerID'] = $newPost->sellerMemberIdMember->idMember;
        $data['post'] = $newPost;
        if ($newPost->type == "ac") {
            Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`auction` (`Post_idPost`, `currentPrice`, `autobidRate`, `minimumBidRate`, `closeDateTime`) VALUES ('" . $newPost->idPost . "', '" . $_POST['price'] . "', 0, '" . $_POST['minimumBidRate'] . "', " . time() . ");")->query();
            $newAuction = Auction::model()->findByPk($newPost->idPost);
            $data['allBid'] = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $newAuction->Post_idPost), array('order' => 'timestamp DESC, price DESC, buyerUsername ASC'));
            $data['auction'] = $newAuction;
        }
        $this->render('index', $data);
    }

    public function actionBuy() {
        if (Yii::app()->user->getState('cID') != NULL && isset($_GET['idPost'])) {
            $post = Post::model()->findByPk($_GET['idPost']);
            if($post->type == 'ds'){
                $post->Buyer_Member_idMember = Yii::app()->user->getState('cID');
                $post->status = 'Purchased';
                $post->save();
            }
            $data['post'] = $post;
            $data['sellerUsername'] = $post->sellerMemberIdMember->username;
            $data['sellerID'] = $post->sellerMemberIdMember->idMember;

            $newInvoice = new Invoice;
            $newInvoice->Post_idPost = $post->idPost;
            $newInvoice->status = 'Wait for Paying';
            //$newInvoice->Buyer_Member_idMember = Yii::app()->user->getState('cID');
            //$newInvoice->Seller_Member_idMember = $post->sellerMemberIdMember->idMember;
            // FALSE means don't have to validate
            $newInvoice->save(FALSE);
        }
        $this->redirect('viewPostDetail?idPost='.$post->idPost);
    }

    public function actionEdit() {
        $post = Post::model()->findByPk($_POST['idPost']);
        if (isset($_POST['productName']) && !empty($_POST['productName']))
            $post->productName = $_POST['productName'];
        if (isset($_POST['productImage']) && !empty($_POST['productImage']))
            $post->productName = $_POST['productName'];
        if (isset($_POST['productDetail']) && !empty($_POST['productDetail']))
            $post->productDetail = $_POST['productDetail'];
        if (isset($_POST['status']) && !empty($_POST['status']))
            $post->status = $_POST['status'];
        if ($post->type == 'ds') {
            if (isset($_POST['price']) && !empty($_POST['price']))
                $post->price = (int) $_POST['price'];
        } else if ($post->type == 'ac') {
            if (isset($_POST['minimumBidRate']) && !empty($_POST['minimumBidRate'])) {
                $auction = Auction::model()->findByPk($_POST['idPost']);
                $auction->minimumBidRate = (int) $_POST['minimumBidRate'];
                $auction->save(FALSE);
            }
        }
        $post->save();
        $data['sellerUsername'] = $post->sellerMemberIdMember->username;
        $data['post'] = $post;
        // now we're startin' file uploading.
        if (isset($_FILES["productImage"])) {
            $file_type = explode(".", $_FILES["productImage"]["name"]);
            $file_type = $file_type[count($file_type) - 1];
            $file_name = $post->idPost . "." . $file_type;
            $destination_path = $_SERVER['DOCUMENT_ROOT'] . Yii::app()->baseUrl . "/clients_upload/" . $file_name;
            if (move_uploaded_file($_FILES["productImage"]["tmp_name"], $destination_path)) {
                $currentPost = Post::model()->findByPk($post->idPost);
                $currentPost->productImage = $file_name;
                $currentPost->save();
                $this->redirect("viewPostDetail?idPost=" . $post->idPost);
            }
        }
        $this->redirect("viewPostDetail?idPost=" . $post->idPost);
    }

    public function actionFormCreate() {
//Hard code please edit
        $data['seller_Member_idMember'] = 1;
        $this->render('formCreate', $data);
    }

    public function actionFormEdit() {
        $data['post'] = Post::model()->findByPk($_GET['idPost']);
        $this->render('formEdit', $data);
    }

    public function actionIndex() {
        if (isset($_POST['idPost']) || isset($idPost) || isset($_GET['idPost'])) {
            if (isset($_POST['idPost']))
                $idPost = $_POST['idPost'];
            if (isset($_GET['idPost']))
                $idPost = $_GET['idPost'];
            $post = Post::model()->findByAttributes(array('idPost' => $idPost));
        }
        if (isset($post)) {
            $data['post'] = $post;
            $data['sellerUsername'] = $post->sellerMemberIdMember->username;
            $data['sellerID'] = $post->sellerMemberIdMember;
            if (isset($post->buyerMemberIdMember)) {
                $data['buyerUsername'] = $post->buyerMemberIdMember->username;
                $data['buyerID'] = $post->buyerMemberIdMember;
            }
            if ($post->type == 'ac') {
                $auction = Auction::model()->findByPk($idPost);
                $data['allBid'] = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $auction->Post_idPost), array('order' => 'timestamp DESC, price DESC, buyerUsername ASC'));
                $data['auction'] = $auction;
                $data['autobid'] = Autobid::model()->findByAttributes(array('idPost' => $_POST['idPost'], 'biderID' => Yii::app()->user->getState('cID')));
            }
        } else {

            $criteria = new CDbCriteria();

            if (isset($_GET['search']) && (!empty($_GET['search']))) {
                $criteria->compare('productName', $_GET['search'], true, "AND");
                $criteria->compare('status', 'open' || 'Purchased', true, "OR");
            } else {
                $criteria->compare('status', 'open', true, "OR");
                $criteria->compare('status', 'Purchased', true, "OR");
            }

            $count = Post::model()->count($criteria);
            $pages = new CPagination($count);

            $pages->pageSize = 15;
            $pages->applyLimit($criteria);

            $sort = new CSort('Post');
            $sort->attributes = array(
                'idPost',
                'productName'
            );
            $sort->defaultOrder = 'postDateTime DESC';
            $sort->applyOrder($criteria);
//   $criteria2 = new CDbCriteria();
//  $criteria2->compare('status' , 'Purchased',true,"OR");
//    $criteria->mergeWith($criteria2);

            $models = Post::model()->findAll($criteria);
            $data['allPost'] = $models;

            $data['pages'] = $pages;
        }
        if (isset($error)) {
            $data['error'] = $error;
        }
        $this->render('index', $data);
    }

    public function actionViewPostList() {
        if (isset($_GET['idMember'])) {
            $data['memberProfile'] = Member::model()->findByPk($_GET['idMember']);
            if (Yii::app()->user->getState("cID") == $_GET['idMember']){
                $data['memberPost'] = Post::model()->findAllByAttributes(array('Seller_Member_idMember' => $_GET['idMember']));
                $data['memberPurchasedPost'] = Post::model()->findAllByAttributes(array('Buyer_Member_idMember' => $_GET['idMember']));
            } else {
                $data['memberPost'] = Post::model()->findAllByAttributes(array('Seller_Member_idMember' => $_GET['idMember']), " `status` = 'Open' OR `status` = 'Purchased'");
                $data['memberPurchasedPost'] = Post::model()->findAllByAttributes(array('Buyer_Member_idMember' => $_GET['idMember']), " `status` = 'Open' OR `status` = 'Purchased'");
            }
            
        } else {
            $data['memberProfile'] = Member::model()->findByPk(Yii::app()->user->getState("cID"));
            $data['memberPost'] = Post::model()->findAllByAttributes(array('Seller_Member_idMember' => Yii::app()->user->getState("cID")), " `status` = 'Open' OR `status` = 'Purchased'");
            $data['memberPurchasedPost'] = Post::model()->findAllByAttributes(array('Buyer_Member_idMember' => Yii::app()->user->getState("cID")), array('order' => 'postDateTime DESC'));
        }

        $data['countPost'] = count($data['memberPost']);
        $data['countPurchasedPost'] = count($data['memberPurchasedPost']);

        //Post
        $criteria = new CDbCriteria();

        if (isset($_GET['idMember'])) {
            $criteria->compare('Seller_Member_idMember', $_GET['idMember'], false, "AND");
            $criteria->compare('status', 'open' || 'Purchased', false, "OR");
        } else {
            $criteria->compare('Seller_Member_idMember', Yii::app()->user->getState("cID"), false, "AND");
            $criteria->compare('status', 'open' || 'Purchased', false, "OR");
        }

        $count = Post::model()->count($criteria);
        $pages = new CPagination($count);

        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $sort = new CSort('Post');
        $sort->attributes = array('idPost');
        $sort->defaultOrder = 'postDateTime DESC';
        $sort->applyOrder($criteria);
        $models = Post::model()->findAll($criteria);
        $data['memberPost'] = $models;

        $data['pages'] = $pages;

        //purchared
        /*
          $criteria_purchased = new CDbCriteria();

          if(isset($_GET['idMember'])){
          $criteria_purchased->compare('Buyer_Member_idMember' , $_GET['idMember'],true,"AND");
          $criteria_purchased->compare('status' , 'open'||'Purchased',true,"OR");
          } else {
          $criteria_purchased->compare('Buyer_Member_idMember' , Yii::app()->user->getState("cID"),true,"AND");
          $criteria_purchased->compare('status' , 'open'||'Purchased',true,"OR");
          }

          $count_purchased = Post::model()->count($criteria_purchased);
          $pages_purchased=new CPagination($count_purchased);

          $pages_purchased->pageSize=2;
          $pages_purchased->applyLimit($criteria_purchased);


          $models_purchased = Post::model()->findAll($criteria_purchased);
          $data['memberPurchasedPost'] = $models_purchased;

          $data['pages_purchased'] = $pages_purchased;
         */

        $this->render('viewPostList', $data);
    }

    public function actionViewPostDetail() {
        if (isset($_POST['idPost']) || isset($idPost) || isset($_GET['idPost'])) {
            if (isset($_POST['idPost']))
                $idPost = $_POST['idPost'];
            if (isset($_GET['idPost']))
                $idPost = $_GET['idPost'];
            $post = Post::model()->findByAttributes(array('idPost' => $idPost));
        }
        if (isset($post)) {
            $data['post'] = $post;
            $data['sellerUsername'] = $post->sellerMemberIdMember->username;
            $data['sellerID'] = $post->sellerMemberIdMember->idMember;
            if (isset($post->buyerMemberIdMember)) {
                $data['buyerUsername'] = $post->buyerMemberIdMember->username;
                $data['buyerID'] = $post->buyerMemberIdMember->idMember;
            }
            if ($post->type == 'ac') {
                $auction = Auction::model()->findByPk($idPost);
                $data['allBid'] = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $auction->Post_idPost), array('order' => 'timestamp DESC, price DESC, buyerUsername ASC'));
                $data['auction'] = $auction;
                $data['autobid'] = Autobid::model()->findByAttributes(array('idPost' => $idPost, 'biderID' => Yii::app()->user->getState('cID')));
            }
        } else {
//                $data['allpostopen'] = Post::model()->findAllByAttributes(array('status'=>"Open"));
//                $data['allpostclose'] = Post::model()->findAllByAttributes(array('status'=>"Purchased"));
            $data['allPost'] = Post::model()->findAll();
        }
        if (isset($error)) {
            $data['error'] = $error;
        }
        $this->render('index', $data);
    }

    public function actionBid() {
        $bid = (int) $_POST['bid'];
        $bidderId = $_POST['idMember'];
        $post = Post::model()->findByAttributes(array('idPost' => $_POST['idPost']));
        $auction = Auction::model()->findByPk($post->idPost);
        $Auction_Post_idPost = $post->idPost;

        $currentPrice = $auction->currentPrice;

        if ($bid >= $auction->minimumBidRate) {
            $currentPrice += $bid;
            $Buyer_Member_idMember = $bidderId;
            $buyerUsername = Member::model()->findByPk($Buyer_Member_idMember)->username;
            Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`bid` (`idBid`, `timestamp`, `Auction_Post_idPost`, `Buyer_Member_idMember`, `price`, `buyerUsername`, `autobid`) 
                VALUES ('',NULL, '" . $Auction_Post_idPost . "', '" . $Buyer_Member_idMember . "', '" . $currentPrice . "', '" . $buyerUsername . "','0');")->query();

            if ($currentPrice > $auction->autobidRate) {
                $post->Buyer_Member_idMember = $bidderId;
            } else if ($auction['topRateID'] != Yii::app()->user->getState('cID')) { //if have autobid from another higher then current price
                $currentPrice = $currentPrice + $auction->minimumBidRate;
                $Buyer_Member_idMember = $auction['topRateID'];
                $buyerUsername = Member::model()->findByPk($Buyer_Member_idMember)->username;
                Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`bid` (`idBid`, `timestamp`, `Auction_Post_idPost`, `Buyer_Member_idMember`, `price`, `buyerUsername`, `autobid`) 
                    VALUES ('',NULL, '" . $Auction_Post_idPost . "', '" . $Buyer_Member_idMember . "', '" . $currentPrice . "', '" . $buyerUsername . "','1');")->query();
            }

            $post->price = $currentPrice;
            $post->save();
            Yii::app()->db->createCommand("UPDATE `psrinth_eshoi`.`auction` SET `currentPrice` = '" . $currentPrice . "' WHERE `auction`.`Post_idPost` = " . $post->idPost . ";")->query();
        } else {
            $has = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $_POST['idPost']));
            if ($has == NULL && $bid == 0) {
                $Buyer_Member_idMember = $bidderId;
                $buyerUsername = Member::model()->findByPk($Buyer_Member_idMember)->username;
                Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`bid` (`idBid`, `timestamp`, `Auction_Post_idPost`, `Buyer_Member_idMember`, `price`, `buyerUsername`, `autobid`) 
                                VALUES ('',NULL, '" . $_POST['idPost'] . "', '" . $Buyer_Member_idMember . "', '" . $currentPrice . "', '" . $buyerUsername . "','0');")->query();
            } else
                $data['error'] = 'Error Please Check';
        }

        if ($post->type == 'ac') {
            $data['allBid'] = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $auction->Post_idPost), array('order' => 'timestamp DESC, price DESC, buyerUsername ASC'));
            $data['auction'] = Auction::model()->findByPk($post->idPost);
        }

        $data['post'] = $post;
        $data['sellerUsername'] = $post->sellerMemberIdMember->username;
        if (isset($post->buyerMemberIdMember))
            $data['buyerUsername'] = $post->buyerMemberIdMember->username;

        $data['sellerID'] = $post->sellerMemberIdMember->idMember;
        $data['autobid'] = Autobid::model()->findByAttributes(array('idPost' => $_POST['idPost'], 'biderID' => Yii::app()->user->getState('cID')));
        $this->render('index', $data);
    }

    public function actionAutobid() {
        $newAutobid = (int) $_POST['autobid']; //new autobid rate 
        $autobidderId = $_POST['idMember'];
        $post = Post::model()->findByAttributes(array('idPost' => $_POST['idPost']));
        $auction = Auction::model()->findByPk($post->idPost);
        $autobid = Autobid::model()->findByAttributes(array('idPost' => $_POST['idPost'], 'biderID' => Yii::app()->user->getState('cID')));
        if (!isset($autobid)) {
            Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`autobid` (`idPost`, `biderId`, `autobidRate`) VALUES ('" . $_POST['idPost'] . "','" . $autobidderId . "','0');")->query();
            $autobid = Autobid::model()->findByAttributes(array('idPost' => $_POST['idPost'], 'biderID' => Yii::app()->user->getState('cID')));
        }
        $currentPrice = $auction->currentPrice;
        $minimumBidRate = $auction->minimumBidRate;
        $oldAutobid = $auction->autobidRate;
        $oldTop = $auction->topRateID;
        if ($newAutobid - $currentPrice >= $minimumBidRate) { //autobid rate higer then mininum rate+currentPrice
            $maxPriceUser = Yii::app()->db->createCommand("SELECT MAX(price),Buyer_Member_idMember FROM `bid` WHERE Auction_Post_idPost =  " . $_POST['idPost'] . " ;")->queryRow();

            if ($oldAutobid < $newAutobid) { //new autobid rate has more then old higher autobid rate
                if ($currentPrice <= $oldAutobid && $oldTop != Yii::app()->user->getState('cID')) //old higher autobid rate has more then current price
                    $currentPrice = $oldAutobid + $minimumBidRate;
                else if ($oldTop != Yii::app()->user->getState('cID') && $maxPriceUser['Buyer_Member_idMember'] != Yii::app()->user->getState('cID')) {
                    $currentPrice +=$minimumBidRate;
                }

                $Auction_Post_idPost = $post->idPost;
                $Buyer_Member_idMember = $autobidderId;
                $buyerUsername = Member::model()->findByPk($Buyer_Member_idMember)->username;
                if ($oldTop != Yii::app()->user->getState('cID'))
                    Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`bid` (`idBid`, `timestamp`, `Auction_Post_idPost`, `Buyer_Member_idMember`, `price`, `buyerUsername`, `autobid`) VALUES ('',NULL, '" . $Auction_Post_idPost . "', '" . $Buyer_Member_idMember . "', '" . $currentPrice . "', '" . $buyerUsername . "','1');")->query();
                $post->Buyer_Member_idMember = $autobidderId;
                Yii::app()->db->createCommand("UPDATE `psrinth_eshoi`.`auction` SET `autobidRate` = '" . $newAutobid . "' , `topRateID` = '" . Yii::app()->user->getState('cID') . "'  WHERE `auction`.`Post_idPost` = " . $post->idPost . ";")->query();
                Yii::app()->db->createCommand("UPDATE `psrinth_eshoi`.`autobid` SET `autobidRate` = '" . $newAutobid . "' WHERE `autobid`.`idPost` = " . $post->idPost . " AND `autobid`.`biderID` = " . Yii::app()->user->getState('cID') . ";")->query();
            } else if (Yii::app()->user->getState('cID') != $oldTop) { //new autobid rate higer then mininum rate+currentPrice    but   less then old  top autobid rate       
                $currentPrice = $newAutobid + $minimumBidRate;
                $Auction_Post_idPost = $post->idPost;
                $Buyer_Member_idMember = $oldTop;
                $buyerUsername = Member::model()->findByPk($Buyer_Member_idMember)->username;
                Yii::app()->db->createCommand("UPDATE `psrinth_eshoi`.`autobid` SET `autobidRate` = '" . $newAutobid . "' WHERE `autobid`.`idPost` = " . $post->idPost . " AND `autobid`.`biderID` = " . Yii::app()->user->getState('cID') . ";")->query();

                //autoid work below
                Yii::app()->db->createCommand("INSERT INTO `psrinth_eshoi`.`bid` (`idBid`, `timestamp`, `Auction_Post_idPost`, `Buyer_Member_idMember`, `price`, `buyerUsername`, `autobid`) VALUES ('',NULL, '" . $Auction_Post_idPost . "', '" . $Buyer_Member_idMember . "', '" . $currentPrice . "', '" . $buyerUsername . "','1');")->query();
                $Buyer_Member_idMember = $post->Buyer_Member_idMember;
            }
            $post->price = $currentPrice;
            $post->save();
            Yii::app()->db->createCommand("UPDATE `psrinth_eshoi`.`auction` SET `currentPrice` = '" . $currentPrice . "' WHERE `auction`.`Post_idPost` = " . $post->idPost . ";")->query();
        } else {
            $data['error'] = 'Error Please Check';
        }

        $data['post'] = $post;
        $data['sellerUsername'] = $post->sellerMemberIdMember->username;
        if (isset($post->buyerMemberIdMember))
            $data['buyerUsername'] = $post->buyerMemberIdMember->username;
        if ($post->type == 'ac') {
            $data['allBid'] = Bid::model()->findAllByAttributes(array('Auction_Post_idPost' => $auction->Post_idPost), array('order' => 'timestamp DESC, price DESC, buyerUsername ASC'));
            $data['auction'] = Auction::model()->findByPk($post->idPost);
        }
        $data['sellerID'] = $post->sellerMemberIdMember->idMember;
        $data['autobid'] = Autobid::model()->findByAttributes(array('idPost' => $_POST['idPost'], 'biderID' => Yii::app()->user->getState('cID')));


        $this->render('index', $data);
    }

    public function actionCancelPost() {
        $idPost = $_GET['idPost'];
        $post = Post::model()->findByAttributes(array('idPost' => $idPost));
        $post->status = "Canceled";
        $post->save();

        $Invoice = Invoice::model()->findByPk($idPost);
        $Invoice->status = "Fail";
        $Invoice->save();

        $this->redirect("index");
    }

// Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
