<?php

class FeedbackController extends Controller
{
    public function actionFeedbackfrombuyer(){
        $feedback = Feedback::model()->findByPk($_POST['idPost']);
        $feedback->feedbackFromBuyer = $_POST['feedbackfrombuyer'];
        $feedback->scoreFromBuyer = $_POST['scorefrombuyer'];
        $feedback->save();

        $invoice = Invoice::model()->findByPk($_POST['idPost']);
        $invoice->status = 'Complete';
        $invoice->save();

        $this->redirect('../post/viewPostDetail?idPost='.$_POST['idPost']);
    }
    
    public function actionFeedbackfromseller(){
        $feedback = new Feedback;
        $feedback->Post_idPost = $_POST['idPost'];
        $feedback->feedbackFromSeller = $_POST['feedbackfromseller'];
        $feedback->scoreFromSeller = $_POST['scorefromseller'];
        $feedback->save();

         $invoice = Invoice::model()->findByPk($_POST['idPost']);
         $invoice->status = 'Wait for Product';
         $invoice->save();

        $this->redirect('../post/viewPostDetail?idPost='.$_POST['idPost']);
    }

	public function actionCreate()
	{
            $newFeedback = new Feedback;
            $newFeedback->Post_idPost = $_POST['idPost'];
            $newFeedback->feedbackFromBuyer = $_POST['feedbackFromBuyer'];
            $newFeedback->feedbackFromSeller = $_POST['feedbackFromBuyer'];
            $newFeedback->Buyer_Member_idMember = $_POST['Buyer_Member_idMember'];
            $newFeedback->Seller_Member_idMember = $_POST['Seller_Member_idMember'];
            // FALSE means don't have to validate
            $newFeedback ->save(FALSE);
            $this->render('index');
	}
        
       	public function actionIndex()
	{
            if (isset($_GET['idPost'])) {
                 $feedback = Feedback::model()->findByPk($_GET['idPost']);
                 $data['buyerUsername'] = $feedback->buyerMemberIdMember->username;
                 $data['sellerUsername'] = $feedback->sellerMemberIdMember->username;
                 $data['feedback'] = $feedback;
                 
            } else {
                $this->render('index');
            }
            $this->render('index',$data);
	}
        
        public function actionFormCreate()
        {
            if (isset($_GET['idPost'])) {
                $post = Post::model()->findByPk($_GET['idPost']);
                $data['sellerUsername'] = $post->sellerMemberIdMember->username;
                if($post->buyerMemberIdMember->username)
                $data['buyerUsername'] = $post->buyerMemberIdMember->username;
                $data['post'] = $post;
            } else {
                $this->render('index');
            }
            $this->render('formCreate',$data);
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}