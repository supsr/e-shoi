<?php

class MemberController extends Controller {

    public function actionReportaccount() {
        $newAbuse = new Abuse;
        $newAbuse->Member_idMember = Yii::app()->user->getState("cID");
        $newAbuse->message = $_POST['message'];
        $newAbuse->referred_idMember = $_POST['referred_idMember'];
        date_default_timezone_set('asia/bangkok');
        $newAbuse->dateReport = date('Y-m-d H:i:s');
        //echo ">>" . $newAbuse->message . " " . $newAbuse->referred_idMember ;
        if (!empty($newAbuse->message)) {
            $newAbuse->save();
            $data['reportAccount'] = 1;
            $this->redirect('viewMemberDetail?idMember=' . $newAbuse->referred_idMember . '&reportAccount=1');
        } else
            $data['reportAccount'] = 0;
        $this->redirect('viewMemberDetail?idMember=' . $newAbuse->referred_idMember . '&reportAccount=0');
    }

    public function actionBan() {
        if (isset($_GET['idMember'])) {
            $id = $_GET['idMember'];
            if (Yii::app()->user->getState("isAdmin")) {
                $bannedMember = Member::model()->findByPk($id);
                $bannedMember->status = 'disable';
                if ($bannedMember->save()) {
                    $data['msg'] = "This account (" . $bannedMember->username . ") has been banned.";
                    $data['typeOfMsg'] = 1;
                } else {
                    $data['msg'] = $bannedMember->getErrors();
                    $data['typeOfMsg'] = 0;
                }
            }
            $this->redirect(array("viewMemberDetail?idMember=" . $id . '&msg=' . $data['msg'] . '&typeOfMsg=' . $data['typeOfMsg']));
        } else {
            $this->redirect(Yii::app()->baseUrl . "/post/index");
        }
    }

    public function actionChangePassword() {

        if (isset($_POST['oldPassword']) && isset($_POST['newPassword']) && isset($_POST['newPassword2'])) {
            $oldPassword = md5($_POST['oldPassword']);
            $newPassword = md5($_POST['newPassword']);
            $newPassword2 = md5($_POST['newPassword2']);
            $id = $_POST['idMember'];
            $editedMember = Member::model()->findByPk($id);
            if (empty($_POST['newPassword']) || strpos($_POST['newPassword'], ' ') || (strlen($_POST['newPassword']) < 6)) {
                $data['msg'] = "New password has incorrect format, please try again.";
                $data['typeOfMsg'] = 0;
            } else if ($oldPassword == $editedMember->password) {
                if ($newPassword == $newPassword2) {
                    // changing password allowed
                    $editedMember->password = $newPassword;
                    if ($editedMember->save()) {
                        $data['msg'] = "Your password has been changed successfully.";
                        $data['typeOfMsg'] = 1;
                    } else {
                        $data['msg'] = $approvedUser->getErrors();
                        $data['typeOfMsg'] = 0;
                    }
                } else {
                    //mismatch on both new passwords
                    $data['msg'] = "New password on both textboxes are not matched.";
                    $data['typeOfMsg'] = 0;
                }
            } else {
                // authentication failed
                $data['msg'] = "Old password is incorrect, please try again.";
                $data['typeOfMsg'] = 0;
            }
        }
        if (isset($_POST['idMember'])) {
            $data['member'] = Member::model()->findByPk($id);
            $this->render("editProfile", $data);
        } else
            $this->redirect(Yii::app()->baseUrl . "/post/index");
    }

    public function actionCreate() {
        $invalid = 0;
        $newMember = new Member;
        $newMember->username = $_POST['username'];
        $newMember->password = md5($_POST['password']);
        $newMember->firstname = $_POST['firstname'];
        $newMember->lastname = $_POST['lastname'];
        $newMember->citizenID = $_POST['citizenID'];
        $newMember->address = $_POST['address'];
        $newMember->country = $_POST['country'];
        $newMember->telephonenumber = $_POST['telephonenumber'];
        $newMember->email = $_POST['email'];
        $data['guest'] = $newMember;

        $error = new Member;
        $usernameChk = Member::model()->findAllBySql("SELECT * FROM member WHERE username = '" . $newMember->username . "'");
        if (count($usernameChk) != 0) {
            $data['usernameDuplicate'] = 1;
            $error->username = 1;
            $invalid = 1;
        }

        if (empty($newMember->username) || strpos($newMember->username, ' ')) {
            $error->username = 1;
            $invalid = 1;
        }
        if (empty($newMember->firstname)) {
            $error->firstname = 1;
            $invalid = 1;
        }
        if (empty($newMember->lastname)) {
            $error->lastname = 1;
            $invalid = 1;
        }
        if (strlen($newMember->citizenID) != 13) {
            $error->citizenID = 1;
            $invalid = 1;
        }
        if (empty($newMember->address)) {
            $error->address = 1;
            $invalid = 1;
        }
        if (empty($newMember->country)) {
            $error->country = 1;
            $invalid = 1;
        }
        if (strlen($newMember->telephonenumber) < 4) {
            $error->telephonenumber = 1;
            $invalid = 1;
        }
        if (empty($newMember->email) || (!strpos($newMember->email, '@')) || (!strpos($newMember->email, '.'))) {
            $error->email = 1;
            $invalid = 1;
        }
        $emailChk = Member::model()->findAllBySql("SELECT * FROM member WHERE email = '" . $newMember->email . "'");
        if (count($emailChk) != 0) {
            $error->email = 1;
            $invalid = 1;
        }
        if (empty($newMember->password) || strpos($_POST['password'], ' ') || (strlen($_POST['password']) < 6)) {
            $error->password = 1;
            $invalid = 1;
        }
        if (!($_POST['password'] == $_POST['repassword'])) {
            $error->password = 1;
            $invalid = 1;
        }
        $data['error'] = $error;

        if ($invalid == 0) {
            $data['invalid'] = 0;
            $data['notice'] = $newMember->save();
            $this->render('register', $data);
        } else {
            $data['invalid'] = 1;
            $this->render('register', $data);
        }
    }

    public function actionEdit() {
        $invalid = 0;
        $Member = Member::model()->findByPk($_POST['idMember']);
        $Member->firstname = $_POST['firstname'];
        $Member->lastname = $_POST['lastname'];
        $Member->citizenID = $_POST['citizenID'];
        $Member->address = $_POST['address'];
        $Member->country = $_POST['country'];
        $Member->telephonenumber = $_POST['telephonenumber'];
        $Member->email = $_POST['email'];

        $error = new Member;
        if (empty($Member->firstname)) {
            $error->firstname = 1;
            $invalid = 1;
        }

        if (empty($Member->lastname)) {
            $error->lastname = 1;
            $invalid = 1;
        }
        if (strlen($Member->citizenID) != 13) {
            $error->citizenID = 1;
            $invalid = 1;
        }
        if (empty($Member->address)) {
            $error->address = 1;
            $invalid = 1;
        }
        if (empty($Member->country)) {
            $error->country = 1;
            $invalid = 1;
        }
        if (strlen($Member->telephonenumber) < 4) {
            $error->telephonenumber = 1;
            $invalid = 1;
        }
        if (empty($Member->email) || (!strpos($Member->email, '@')) || (!strpos($Member->email, '.'))) {
            $error->email = 1;
            $invalid = 1;
        }
        $data['error'] = $error;

        if ($invalid == 0) {
            $data['notice'] = $Member->save();
            $data['memberProfile'] = $Member;
            $this->render('viewMemberDetail', $data);
        } else {
            $data['invalid'] = 1;
            $data['member'] = $Member;
            $this->render('editProfile', $data);
        }
    }

    public function actionIndex() {
        if (isset($notice)) {
            $data['notice'] = $notice;
        }
        if (!isset($_GET['sent'])) {
            $sent = false;
        } else {
            $sent = true;
        }
        if (Yii::app()->user->getState('isAdmin', false)) {
            if (isset($_GET['idMember']) && isset($_GET['approve'])) { //get approve or not approve in this member (admin choice)
                $data['resultApproval'] = Approver::approve(intval($_GET['approve']) == 1, intval($_GET['idMember']));
                if (!$sent && ($data['resultApproval']['typeOfMsg'] == 1) && (intval($_GET['approve']) == 1)) {
                    $this->redirect("sendEmail?idMember=" . $_GET['idMember'] . "&typeOfMsg=" . $data['resultApproval']['typeOfMsg'] . "&msg=" . $data['resultApproval']['msg']);
                }
            }
            $data['pendingAccounts'] = Member::model()->findAllByAttributes(array('status' => 'unactivated'));
            $AbuseList = Abuse::model()->findAll();
            $data['reportingAccounts'] = $AbuseList;
        }

        $criteria = new CDbCriteria();

        if (isset($_GET['search']) && (!empty($_GET['search']))) {
            $criteria->compare('username', $_GET['search'], true, "AND");
        }

        $criteria->compare('status', 'enable', true, "AND");

        $count = Member::model()->count($criteria);
        $pages = new CPagination($count);

        $pages->pageSize = 20;
        $pages->applyLimit($criteria);

        $sort = new CSort('Member');
        $sort->attributes = array('username');
        $sort->applyOrder($criteria);
//   $criteria2 = new CDbCriteria();
//  $criteria2->compare('status' , 'Purchased',true,"OR");
//    $criteria->mergeWith($criteria2);

        $models = Member::model()->findAll($criteria);

        $data['allUser'] = $models;

        $data['pages'] = $pages;


        /*
          if(isset($_GET['search']))
          {$data['allUser'] = Member::model()->findAllBySql("SELECT * FROM `Member` WHERE username LIKE '%".$_GET['search']."%'" );}
          else if(isset($_POST['search']))
          {$data['allUser'] = Member::model()->findAllBySql("SELECT * FROM `Member` WHERE username LIKE '%".$_POST['search']."%'" );}
          else {$data['allUser'] = Member::model()->findAll();}
         */
        
        $this->render('index', $data);
    }

    public function actionFormCreate() {
        $this->render('formCreate');
    }

    public function actionFormEdit() {
        if (isset($_GET['idMember'])) {
            $data['member'] = Member::model()->findByPk($_GET['idMember']);
            $this->render('formEdit', $data);
        } else {
            $this->run('index');
        }
    }

    public function actionViewMemberDetail() {
        if (Yii::app()->user->getState('isAdmin', false)) {
            if (isset($_GET['idMember']) && isset($_GET['approve'])) {
                $data['resultApproval'] = Approver::approve(intval($_GET['approve']) == 1, intval($_GET['idMember']));
            }
        }
        if (isset($_GET['idMember'])) {
            $data['memberProfile'] = Member::model()->findByPk($_GET['idMember']);
            $data['memberPost'] = Post::model()->findAllByAttributes(array('Seller_Member_idMember' => $_GET['idMember']));
            $data['memberPurchasedPost'] = Post::model()->findAllByAttributes(array('Buyer_Member_idMember' => $_GET['idMember']));
        } else {
            $data['memberProfile'] = Member::model()->findByPk(Yii::app()->user->getState("cID"));
            $data['memberPost'] = Post::model()->findAllByAttributes(array('Seller_Member_idMember' => Yii::app()->user->getState("cID")));
            $data['memberPurchasedPost'] = Post::model()->findAllByAttributes(array('Buyer_Member_idMember' => Yii::app()->user->getState("cID")));
        }

        $data['countPost'] = count($data['memberPost']);
        $data['countPurchasedPost'] = count($data['memberPurchasedPost']);
        $this->render('viewMemberDetail', $data);

        // $this->renderPartial('/post/viewPostList', $data);
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
    public function actionRegister() {
        /*
          $model = new Member;
          // uncomment the following code to enable ajax-based validation

          if (isset($_POST['ajax']) && $_POST['ajax'] === 'account-Register-form') {
          echo CActiveForm::validate($model);
          Yii::app()->end();
          }


          if (isset($_POST['Account'])) {
          $model->attributes = $_POST['Account'];
          if ($model->validate()) {
          $model->save();

          $modelLogin = new LoginForm;
          $modelLogin->username = $model->username;
          $modelLogin->password = $model->password;

          // validate user input and redirect to the previous page if valid
          if ($modelLogin->validate() && $modelLogin->login())
          $this->redirect(Yii::app()->user->returnUrl);

          // display the login form
          $this->render('login', array('model' => $modelLogin));
          // form inputs are valid, do something here
          }
          }

          //$this->render('Register',array('model'=>$model));
          $this->render('register', array('model' => $model));
         */
        $this->render('register');
    }

    public function actionSendEmail() {

        $passedTypeOfMsg = $_GET['typeOfMsg'];
        $passedMsg = $_GET['msg'];
        $receiver = Member::model()->findByPk($_GET['idMember']);
        //$receiver = Member::model()->findByPk(5);
        $to = $receiver->email;
        $subject = "Your registration has been approved on " . Yii::app()->name;

        $message = $this->renderPartial('sendEmail', null, true);

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= 'To: ' . $receiver->firstname . ' ' . $receiver->lastname . ' <' . $to . '>\r\n';
        $headers .= 'From: ' . Yii::app()->name . '\'s auto mailing <' . Yii::app()->params['adminEmail'] . '>' . "\r\n";
        $resultSend = mail($to, $subject, $message, $headers);

        $this->redirect('index?approve=1&sent=1&idMember=' . $_GET['idMember']);
    }

    public function actionEditProfile() {
        if (Yii::app()->user->getState('cID') != null) {
            $data['member'] = Member::model()->findByPk(Yii::app()->user->getState('cID'));
            $this->render('editProfile', $data);
        } else {
            $this->run('index');
        }
    }

}
