<?php

/**
 * This is the model class for table "bid".
 *
 * The followings are the available columns in table 'bid':
 * @property integer $idBid
 * @property string $timestamp
 * @property integer $Auction_Post_idPost
 * @property integer $Buyer_Member_idMember
 * @property double $price
 * @property string $buyerUsername
 * @property integer $autobid
 *
 * The followings are the available model relations:
 * @property Auction $auctionPostIdPost
 * @property Member $buyerMemberIdMember
 */
class Bid extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bid';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Auction_Post_idPost, Buyer_Member_idMember, autobid', 'required'),
			array('Auction_Post_idPost, Buyer_Member_idMember, autobid', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('timestamp', 'length', 'max'=>6),
			array('buyerUsername', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idBid, timestamp, Auction_Post_idPost, Buyer_Member_idMember, price, buyerUsername, autobid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'auctionPostIdPost' => array(self::BELONGS_TO, 'Auction', 'Auction_Post_idPost'),
			'buyerMemberIdMember' => array(self::BELONGS_TO, 'Member', 'Buyer_Member_idMember'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idBid' => 'Id Bid',
			'timestamp' => 'Timestamp',
			'Auction_Post_idPost' => 'Auction Post Id Post',
			'Buyer_Member_idMember' => 'Buyer Member Id Member',
			'price' => 'Price',
			'buyerUsername' => 'Buyer Username',
			'autobid' => 'Autobid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idBid',$this->idBid);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('Auction_Post_idPost',$this->Auction_Post_idPost);
		$criteria->compare('Buyer_Member_idMember',$this->Buyer_Member_idMember);
		$criteria->compare('price',$this->price);
		$criteria->compare('buyerUsername',$this->buyerUsername,true);
		$criteria->compare('autobid',$this->autobid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
