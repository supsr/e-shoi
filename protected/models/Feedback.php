<?php

/**
 * This is the model class for table "feedback".
 *
 * The followings are the available columns in table 'feedback':
 * @property integer $Post_idPost
 * @property string $feedbackFromBuyer
 * @property string $feedbackFromSeller
 * @property integer $scoreFromBuyer
 * @property integer $scoreFromSeller
 *
 * The followings are the available model relations:
 * @property Post $postIdPost
 */
class Feedback extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'feedback';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Post_idPost, scoreFromSeller', 'required'),
			array('Post_idPost, scoreFromBuyer, scoreFromSeller', 'numerical', 'integerOnly'=>true),
			array('feedbackFromBuyer, feedbackFromSeller', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Post_idPost, feedbackFromBuyer, feedbackFromSeller, scoreFromBuyer, scoreFromSeller', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'postIdPost' => array(self::BELONGS_TO, 'Post', 'Post_idPost'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Post_idPost' => 'Post Id Post',
			'feedbackFromBuyer' => 'Feedback From Buyer',
			'feedbackFromSeller' => 'Feedback From Seller',
			'scoreFromBuyer' => 'Score From Buyer',
			'scoreFromSeller' => 'Score From Seller',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Post_idPost',$this->Post_idPost);
		$criteria->compare('feedbackFromBuyer',$this->feedbackFromBuyer,true);
		$criteria->compare('feedbackFromSeller',$this->feedbackFromSeller,true);
		$criteria->compare('scoreFromBuyer',$this->scoreFromBuyer);
		$criteria->compare('scoreFromSeller',$this->scoreFromSeller);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Feedback the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
