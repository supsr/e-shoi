<?php

/**
 * This is the model class for table "auction".
 *
 * The followings are the available columns in table 'auction':
 * @property integer $Post_idPost
 * @property double $currentPrice
 * @property double $autobidRate
 * @property double $minimumBidRate
 * @property string $closeDateTime
 *
 * The followings are the available model relations:
 * @property Post $postIdPost
 * @property Bid[] $bs
 */
class Auction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'auction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Post_idPost, currentPrice', 'required'),
			array('Post_idPost', 'numerical', 'integerOnly'=>true),
			array('currentPrice, autobidRate, minimumBidRate', 'numerical'),
			array('closeDateTime', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Post_idPost, currentPrice, autobidRate, minimumBidRate, closeDateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'postIdPost' => array(self::BELONGS_TO, 'Post', 'Post_idPost'),
			'bs' => array(self::HAS_MANY, 'Bid', 'Auction_Post_idPost'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Post_idPost' => 'Post Id Post',
			'currentPrice' => 'Current Price',
			'autobidRate' => 'Autobid Rate',
			'minimumBidRate' => 'Minimum Bid Rate',
			'closeDateTime' => 'Close Date Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Post_idPost',$this->Post_idPost);
		$criteria->compare('currentPrice',$this->currentPrice);
		$criteria->compare('autobidRate',$this->autobidRate);
		$criteria->compare('minimumBidRate',$this->minimumBidRate);
		$criteria->compare('closeDateTime',$this->closeDateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Auction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
