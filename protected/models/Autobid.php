<?php

/**
 * This is the model class for table "autobid".
 *
 * The followings are the available columns in table 'autobid':
 * @property integer $idPost
 * @property integer $biderID
 * @property integer $autobidRate
 */
class Autobid extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Autobid the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'autobid';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPost, biderID, autobidRate', 'required'),
			array('idPost, biderID, autobidRate', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPost, biderID, autobidRate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPost' => 'Id Post',
			'biderID' => 'Bider',
			'autobidRate' => 'Autobid Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPost',$this->idPost);
		$criteria->compare('biderID',$this->biderID);
		$criteria->compare('autobidRate',$this->autobidRate);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}