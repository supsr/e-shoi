<?php

/**
 * This is the model class for table "abuse".
 *
 * The followings are the available columns in table 'abuse':
 * @property integer $idAbuse
 * @property string $message
 * @property integer $Member_idMember
 * @property integer $Post_idPost
 * @property integer $referred_idMember
 * @property string $dateReport
 *
 * The followings are the available model relations:
 * @property Member $memberIdMember
 * @property Post $postIdPost
 */
class Abuse extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'abuse';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('referred_idMember, dateReport', 'required'),
			array('Member_idMember, Post_idPost, referred_idMember', 'numerical', 'integerOnly'=>true),
			array('message', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idAbuse, message, Member_idMember, Post_idPost, referred_idMember, dateReport', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'memberIdMember' => array(self::BELONGS_TO, 'Member', 'Member_idMember'),
			'postIdPost' => array(self::BELONGS_TO, 'Post', 'Post_idPost'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idAbuse' => 'Id Abuse',
			'message' => 'Message',
			'Member_idMember' => 'Member Id Member',
			'Post_idPost' => 'Post Id Post',
			'referred_idMember' => 'Referred Id Member',
			'dateReport' => 'Date Report',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idAbuse',$this->idAbuse);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('Member_idMember',$this->Member_idMember);
		$criteria->compare('Post_idPost',$this->Post_idPost);
		$criteria->compare('referred_idMember',$this->referred_idMember);
		$criteria->compare('dateReport',$this->dateReport,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Abuse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
