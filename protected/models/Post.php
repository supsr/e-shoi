<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $idPost
 * @property string $type
 * @property string $productName
 * @property string $productDetail
 * @property string $productImage
 * @property string $status
 * @property integer $Buyer_Member_idMember
 * @property integer $Seller_Member_idMember
 * @property double $price
 *
 * The followings are the available model relations:
 * @property Abuse[] $abuses
 * @property Auction $auction
 * @property Feedback $feedback
 * @property Invoice $invoice
 * @property Member $buyerMemberIdMember
 * @property Member $sellerMemberIdMember
 */
class Post extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Seller_Member_idMember', 'required'),
			array('Buyer_Member_idMember, Seller_Member_idMember', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('type', 'length', 'max'=>2),
			array('productName', 'length', 'max'=>45),
			array('productImage', 'length', 'max'=>200),
			array('status', 'length', 'max'=>9),
			array('productDetail', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idPost, type, productName, productDetail, productImage, status, Buyer_Member_idMember, Seller_Member_idMember, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'abuses' => array(self::HAS_MANY, 'Abuse', 'Post_idPost'),
			'auction' => array(self::HAS_ONE, 'Auction', 'Post_idPost'),
			'feedback' => array(self::HAS_ONE, 'Feedback', 'Post_idPost'),
			'invoice' => array(self::HAS_ONE, 'Invoice', 'Post_idPost'),
			'buyerMemberIdMember' => array(self::BELONGS_TO, 'Member', 'Buyer_Member_idMember'),
			'sellerMemberIdMember' => array(self::BELONGS_TO, 'Member', 'Seller_Member_idMember'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPost' => 'Id Post',
			'type' => 'Type',
			'productName' => 'Product Name',
			'productDetail' => 'Product Detail',
			'productImage' => 'Product Image',
			'status' => 'Status',
			'Buyer_Member_idMember' => 'Buyer Member Id Member',
			'Seller_Member_idMember' => 'Seller Member Id Member',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPost',$this->idPost);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('productName',$this->productName,true);
		$criteria->compare('productDetail',$this->productDetail,true);
		$criteria->compare('productImage',$this->productImage,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('Buyer_Member_idMember',$this->Buyer_Member_idMember);
		$criteria->compare('Seller_Member_idMember',$this->Seller_Member_idMember);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
