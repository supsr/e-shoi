<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $idMember
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 * @property string $country
 * @property string $telephonenumber
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $status
 * @property string $type
 *
 * The followings are the available model relations:
 * @property Abuse[] $abuses
 * @property Bid[] $bs
 * @property Feedback[] $feedbacks
 * @property Feedback[] $feedbacks1
 * @property Invoice[] $invoices
 * @property Invoice[] $invoices1
 * @property Post[] $posts
 * @property Post[] $posts1
 */
class Member extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstname, lastname, address, country, telephonenumber, username, password, email', 'required'),
			array('firstname, lastname, address, country, telephonenumber, username, password, email', 'length', 'max'=>45),
			array('status', 'length', 'max'=>11),
			array('type', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idMember, firstname, lastname, address, country, telephonenumber, username, password, email, status, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'abuses' => array(self::HAS_MANY, 'Abuse', 'Member_idMember'),
			'bs' => array(self::HAS_MANY, 'Bid', 'Buyer_Member_idMember'),
			'feedbacks' => array(self::HAS_MANY, 'Feedback', 'Buyer_Member_idMember'),
			'feedbacks1' => array(self::HAS_MANY, 'Feedback', 'Seller_Member_idMember'),
			'invoices' => array(self::HAS_MANY, 'Invoice', 'Buyer_Member_idMember'),
			'invoices1' => array(self::HAS_MANY, 'Invoice', 'Seller_Member_idMember'),
			'posts' => array(self::HAS_MANY, 'Post', 'Buyer_Member_idMember'),
			'posts1' => array(self::HAS_MANY, 'Post', 'Seller_Member_idMember'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idMember' => 'Id Member',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'address' => 'Address',
			'country' => 'Country',
			'telephonenumber' => 'Telephone number',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'status' => 'Status',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idMember',$this->idMember);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('telephonenumber',$this->telephonenumber,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
